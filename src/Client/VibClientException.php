<?php

namespace Drupal\vib_service\Client;

/**
 * Class VibClientException
 * @package Drupal\vib_service\Client
 */
class VibClientException extends \Exception {

  /**
   * VibClientException constructor.
   *
   * @param $message
   * @param int $code
   * @param \Exception|NULL $previous
   */
  public function __construct($message, $code = 0, \Exception $previous = NULL) {
    parent::__construct('Invalid VIB services call: ' . $message, $code, $previous);

    // Log in datadog.
    /** @var \Drupal\vib_service\Logger\DatadogLog $logger */
    $logger = \Drupal::service('vib_service.datadog_log');
    $logger->error($message, ['channel' => 'VIB services API']);
  }

}
