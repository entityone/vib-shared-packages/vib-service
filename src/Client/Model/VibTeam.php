<?php

namespace Drupal\vib_service\Client\Model;

use Drupal\Core\Field\FieldFilteredMarkup;

/**
 * Defines the VIB team model.
 */
class VibTeam implements VibObjectInterface, VibFieldableObjectInterface {

  /**
   * The ID.
   *
   * @var string|int
   */
  protected $id;

  /**
   * The team name.
   *
   * @var string
   */
  protected string $name;

  /**
   * The team machine name.
   *
   * @var string
   */
  protected string $machineName;

  /**
   * VibRole constructor.
   * @param string|int $id
   * @param string $name
   */
  public function __construct($id, string $name) {
    $this->id = $id;
    $this->name = $name;
    $this->machineName = self::toMachineName($this->name);
  }

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @return string
   */
  public function getMachineName() {
    return $this->machineName;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldLabel() {
    return $this->getName();
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldKey() {
    return $this->getId();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      '#markup' => $this->getName(),
      '#allowed_tags' => FieldFilteredMarkup::allowedTags(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'Id' => $this->getId(),
      'Name' => $this->getName(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static($json['Id'], $json['Name']);
  }

  /**
   * @param $name
   * @return string
   */
  public static function toMachineName($name){
    return str_replace('.', '_', $name);
  }

}
