<?php

namespace Drupal\vib_service\Client\Model;


/**
 * Class VibUserResponse
 * @package Drupal\vib_service\Client\Model
 */
class VibUserResponse implements VibObjectInterface {

  protected $id;
  protected $title;
  protected $firstName;
  protected $lastName;
  protected $nickName;
  protected $email;
  protected $phone;
  protected $publicPhone;
  protected $linkedInUrl;
  protected $twitterUrl;
  protected $instagramUrl;
  protected $facebookUrl;
  protected $orcidId;
  protected $company;
  protected $department;
  protected $jobTitle;
  protected $privacy;
  protected $profilePictureUrl;
  protected $roles;
  protected $teams;
  protected $billingAddress;
  protected $shippingAddress;
  protected $vibInfo;
  protected $metaData;
  protected $centres;
  protected $isNew;
  protected $activationToken;

  /**
   * VibUserResponse constructor.
   * @param $id
   * @param $title
   * @param $first_name
   * @param $last_name
   * @param $nick_name
   * @param $email
   * @param $phone
   * @param $public_phone
   * @param $linked_in_url
   * @param $twitter_url
   * @param $instagram_url
   * @param $facebook_url
   * @param $orcid_id
   * @param $company
   * @param $job_title
   * @param $privacy
   * @param $profile_picture_url
   * @param array $roles
   * @param array $teams
   * @param \Drupal\vib_service\Client\Model\VibUserBillingAddress $billing_address
   * @param \Drupal\vib_service\Client\Model\VibUserShippingAddress $shipping_address
   * @param \Drupal\vib_service\Client\Model\VibUserVibInfo $vib_info
   * @param \Drupal\vib_service\Client\Model\VibMetaData $data
   * @param array $centres
   * @param null $activation_token
   */
  public function __construct(
    $id,
    $title,
    $first_name,
    $last_name,
    $nick_name,
    $email,
    $phone,
    $public_phone,
    $linked_in_url,
    $twitter_url,
    $instagram_url,
    $facebook_url,
    $orcid_id,
    $company,
    $department,
    $job_title,
    $privacy,
    $profile_picture_url,
    array $roles,
    array $teams,
    VibUserBillingAddress $billing_address,
    VibUserShippingAddress $shipping_address,
    VibUserVibInfo $vib_info,
    VibMetaData $data,
    array $centres,
    $activation_token = NULL
  ) {
    $this->id = $id;
    $this->title = $title;
    $this->firstName = $first_name;
    $this->lastName = $last_name;
    $this->nickName = $nick_name;
    $this->email = $email;
    $this->phone = $phone;
    $this->publicPhone = $public_phone;
    $this->linkedInUrl = $linked_in_url;
    $this->twitterUrl = $twitter_url;
    $this->instagramUrl = $instagram_url;
    $this->facebookUrl = $facebook_url;
    $this->orcidId = $orcid_id;
    $this->company = $company;
    $this->department = $department;
    $this->jobTitle = $job_title;
    $this->privacy = $privacy;
    $this->profilePictureUrl = $profile_picture_url;
    $this->roles = $roles;
    $this->teams = $teams;
    $this->billingAddress = $billing_address;
    $this->shippingAddress = $shipping_address;
    $this->vibInfo = $vib_info;
    $this->metaData = $data;
    $this->centres = $centres;
    $this->activationToken = $activation_token;
  }

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @return string
   */
  public function getFirstName() {
    return $this->firstName;
  }

  /**
   * @return string
   */
  public function getLastName() {
    return $this->lastName;
  }

  /**
   * @return string
   */
  public function getNickName() {
    return $this->nickName;
  }

  /**
   * @return mixed
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * @return string
   */
  public function getPhone() {
    return $this->phone;
  }

  /**
   * @return string
   */
  public function getPublicPhone() {
    return $this->publicPhone;
  }

  /**
   * @return string
   */
  public function getLinkedInUrl() {
    return $this->linkedInUrl;
  }

  /**
   * @return string
   */
  public function getTwitterUrl() {
    return $this->twitterUrl;
  }

  /**
   * @return string
   */
  public function getInstagramUrl() {
    return $this->instagramUrl;
  }

  /**
   * @return string
   */
  public function getFacebookUrl() {
    return $this->facebookUrl;
  }

  /**
   * @return string
   */
  public function getOrcidId() {
    return $this->orcidId;
  }

  /**
   * @return string
   */
  public function getCompany() {
    return $this->company;
  }

  /**
   * @return string
   */
  public function getDepartment() {
    return $this->department;
  }

  /**
   * @return string
   */
  public function getJobTitle() {
    return $this->jobTitle;
  }

  /**
   * @return string
   */
  public function getPrivacy() {
    return $this->privacy;
  }

  /**
   * @return bool
   */
  public function privacyIsPublic() {
    return $this->privacy === 'Public';
  }

  /**
   * @return bool
   */
  public function privacyIsRestricted() {
    return $this->privacy === 'Restricted';
  }

  /**
   * @return bool
   */
  public function privacyIsPrivate() {
    return $this->privacy === 'Private';
  }

  /**
   * @return string
   */
  public function getProfilePictureUrl() {
    return $this->profilePictureUrl;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibRole[]
   */
  public function getRoles() {
    return $this->roles;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibTeam[]
   */
  public function getTeams() {
    return $this->teams;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibUserBillingAddress
   */
  public function getBillingAddress() {
    return $this->billingAddress;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibUserShippingAddress
   */
  public function getShippingAddress() {
    return $this->shippingAddress;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibUserVibInfo
   */
  public function getVibInfo() {
    return $this->vibInfo;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibMetaData
   */
  public function getMetaData() {
    return $this->metaData;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibCentre[]
   */
  public function getCentres() {
    return $this->centres;
  }

  /**
   * @return string
   */
  public function getActivationToken() {
    return $this->activationToken;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'Id' => $this->getId(),
      'Title' => $this->getTitle(),
      'FirstName' => $this->getFirstName(),
      'LastName' => $this->getLastName(),
      'NickName' => $this->getNickName(),
      'Email' => $this->getEmail(),
      'Phone' => $this->getPhone(),
      'PublicPhone' => $this->getPublicPhone(),
      'LinkedInUrl' => $this->getLinkedInUrl(),
      'TwitterUrl' => $this->getTwitterUrl(),
      'InstagramUrl' => $this->getInstagramUrl(),
      'FacebookUrl' => $this->getFacebookUrl(),
      'OrcidId' => $this->getOrcidId(),
      'Company' => $this->getCompany(),
      'Department' => $this->getDepartment(),
      'JobTitle' => $this->getJobTitle(),
      'Privacy' => $this->getPrivacy(),
      'ProfilePicUrl' => $this->getProfilePictureUrl(),
      'BillingAddress' => $this->getBillingAddress()->toJson(),
      'ShippingAddress' => $this->getShippingAddress()->toJson(),
      'VIBInfo' => $this->getVibInfo()->toJson(),
      'Roles' => array_map(function (VibRole $role) {
        return $role->toJson();
      }, $this->getRoles()),
      'Centres' => array_map(function (VibCentre $centre) {
        return $centre->toJson();
      }, $this->getCentres()),
    ];
  }

  /**
   * @param $user_info
   */
  public function alterOpenIdUserInfo(&$user_info) {
    // These keys have to match the keys of the claims.
    $user_info['vib_user_title'] = $this->getTitle();
    $user_info['vib_user_first_name'] = $this->getFirstName();
    $user_info['vib_user_last_name'] = $this->getLastName();
    $user_info['vib_user_nick_name'] = $this->getNickName();
    $user_info['vib_user_phone'] = $this->getPhone();
    $user_info['vib_user_linked_in_url'] = $this->getLinkedInUrl();
    $user_info['vib_user_twitter_url'] = $this->getTwitterUrl();
    $user_info['vib_user_instagram_url'] = $this->getInstagramUrl();
    $user_info['vib_user_facebook_url'] = $this->getFacebookUrl();
    $user_info['vib_user_orcid_id'] = $this->getOrcidId();
    $user_info['vib_user_company'] = $this->getCompany();
    $user_info['vib_user_department'] = $this->getDepartment();
    $user_info['vib_user_profile_picture'] = $this->getProfilePictureUrl();

    $this->getBillingAddress()->alterOpenIdUserInfo($user_info);
    $this->getShippingAddress()->alterOpenIdUserInfo($user_info);
    $this->getVibInfo()->alterOpenIdUserInfo($user_info);
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    $roles = [];
    foreach ($json['Roles'] as $role) {
      $roles[] = new VibRole($role, $role);
    }

    $teams = [];
    if (isset($json['Teams'])) {
      foreach ($json['Teams'] as $team) {
        $teams[] = new VibTeam($team['Id'], $team['Name']);
      }
    }

    $centres = [];
    if (!empty($json['Centers'])) {
      foreach ($json['Centers'] as $centre) {
        $centres[] = VibCentre::createFromJson($centre);
      }
    }

    return new static(
      $json['Id'],
      $json['Title'],
      $json['FirstName'],
      $json['LastName'],
      $json['NickName'] ?? NULL,
      $json['Email'],
      $json['Phone'],
      $json['PublicPhone'],
      $json['LinkedInUrl'],
      $json['TwitterUrl'],
      $json['InstagramUrl'] ?? NULL,
      $json['FacebookUrl'] ?? NULL,
      $json['OrcidId'] ?? NULL,
      $json['Company'],
      $json['Department'] ?? NULL,
      $json['JobTitle'] ?? NULL,
      $json['Privacy'] ?: 'Restricted',
      $json['ProfilePicUrl'],
      $roles,
      $teams,
      VibUserBillingAddress::createFromJson($json['BillingAddress']),
      VibUserShippingAddress::createFromJson($json['ShippingAddress']),
      VibUserVibInfo::createFromJson($json['VIBInfo']),
      new VibMetaData($json['Metadata']),
      $centres,
      $json['ActivationToken'] ?? NULL
    );
  }

  /**
   * Returns the claims available in VIB Service.
   *
   * @param $claims
   * @return array
   */
  public static function claims($claims) {
    $new_claims = [
      'name' => $claims['name'],
      'email' => $claims['email'],
    ];

    $new_claims['vib_user_title'] = [
      'scope' => 'profile',
      'title' => 'Title',
      'type' => 'string',
    ];

    $new_claims['vib_user_first_name'] = [
      'scope' => 'profile',
      'title' => 'First name',
      'type' => 'string',
    ];

    $new_claims['vib_user_last_name'] = [
      'scope' => 'profile',
      'title' => 'Last name',
      'type' => 'string',
    ];

    $new_claims['vib_user_nick_name'] = [
      'scope' => 'profile',
      'title' => 'Nickname',
      'type' => 'string',
    ];

    $new_claims['vib_user_phone'] = [
      'scope' => 'profile',
      'title' => 'Phone',
      'type' => 'string',
    ];

    $new_claims['vib_user_linked_in_url'] = [
      'scope' => 'profile',
      'title' => 'LinkedIn URL',
      'type' => 'string',
    ];

    $new_claims['vib_user_company'] = [
      'scope' => 'profile',
      'title' => 'Company',
      'type' => 'string',
    ];

    $new_claims['vib_user_department'] = [
      'scope' => 'profile',
      'title' => 'Department',
      'type' => 'string',
    ];

    $new_claims['vib_user_profile_picture'] = [
      'scope' => 'profile',
      'title' => 'Profile picture',
      'type' => 'image',
    ];

    VibUserBillingAddress::claims($new_claims);
    VibUserShippingAddress::claims($new_claims);
    VibUserVibInfo::claims($new_claims);

    return $new_claims;
  }

}
