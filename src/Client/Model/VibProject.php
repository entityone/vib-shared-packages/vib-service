<?php

namespace Drupal\vib_service\Client\Model;

/**
 * Class VibProject.
 *
 * @package Drupal\vib_service\Client\Model
 */
class VibProject implements VibObjectInterface {

  protected $centerCode;
  protected $centerName;
  protected $projectCode;
  protected $projectName;

  /**
   * VibProject constructor.
   * @param string $centerCode
   * @param string $centerName
   * @param string $projectCode
   * @param string $projectName
   */
  public function __construct($centerCode, $centerName, $projectCode, $projectName) {
    $this->centerCode = $centerCode;
    $this->centerName = $centerName;
    $this->projectCode = $projectCode;
    $this->projectName = $projectName;
  }

  /**
   * @return string
   */
  public function getCenterCode(): string {
    return $this->centerCode;
  }

  /**
   * @return string
   */
  public function getCenterName(): string {
    return $this->centerName;
  }

  /**
   * @return string
   */
  public function getCode(): string {
    return $this->projectCode;
  }

  /**
   * @return string
   */
  public function getName(): string {
    return $this->projectName;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'ProjectName' => $this->getName(),
      'ProjectCode' => $this->getCode(),
      'CenterName' => $this->getCenterName(),
      'CenterCode' => $this->getCenterCode(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static(
      $json['CenterCode'],
      $json['CenterName'],
      $json['ProjectCode'],
      $json['ProjectName']
    );
  }


}
