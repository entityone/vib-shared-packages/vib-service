<?php

namespace Drupal\vib_service\Client\Model;

/**
 * Interface VibObjectInterface
 * @package Drupal\vib_service\Client\Model
 */
interface VibObjectInterface {

  /**
   * @return array
   */
  public function toJson();

  /**
   * @param array $json
   * @return \Drupal\vib_service\Client\Model\VibObjectInterface
   */
  public static function createFromJson(array $json);

}