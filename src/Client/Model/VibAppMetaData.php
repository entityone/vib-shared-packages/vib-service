<?php

namespace Drupal\vib_service\Client\Model;

/**
 * Class VibAppMetaData
 * @package Drupal\vib_service\Client\Model
 */
class VibAppMetaData implements VibObjectInterface {

  protected $name;
  protected $type;
  protected $label;
  protected $helpText;
  protected $options;
  protected $isRequired;
  protected $isHidden;

  /**
   * VibAppMetaData constructor.
   * @param $name
   * @param $type
   * @param $label
   * @param $help_text
   * @param array $options
   * @param $is_required
   * @param $is_hidden
   */
  public function __construct($name, $type, $label, $help_text, array $options, $is_required, $is_hidden) {
    $this->name = $name;
    $this->type = $type;
    $this->label = $label;
    $this->helpText = $help_text;
    $this->options = $options;
    $this->isRequired = $is_required;
    $this->isHidden = $is_hidden;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @return string
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * @return string
   */
  public function getHelpText() {
    return $this->helpText;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibAppMetaDataOption[]
   */
  public function getOptions() {
    return $this->options;
  }

  /**
   * @return boolean
   */
  public function isRequired() {
    return $this->isRequired;
  }

  /**
   * @return boolean
   */
  public function isHidden() {
    return $this->isHidden;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    $json = [
      'Name' => $this->getName(),
      'Type' => $this->getType(),
      'Label' => $this->getLabel(),
      'HelpText' => $this->getHelpText(),
      'Required' => $this->isRequired(),
      'Hidden' => $this->isHidden(),
    ];

    foreach ($this->getOptions() as $option) {
      $json['Options'][] = $option->toJson();
    }

    return $json;
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    $options = [];

    if (!empty($json['Options'])) {
      $options = array_map(function (array $option) {
        return VibAppMetaDataOption::createFromJson($option);
      }, $json['Options']);
    }

    return new static(
      $json['Name'],
      $json['Type'],
      $json['Label'],
      $json['HelpText'],
      $options,
      $json['Required'],
      $json['Hidden']
    );
  }


}
