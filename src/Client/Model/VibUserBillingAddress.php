<?php

namespace Drupal\vib_service\Client\Model;

/**
 * Class VibUserBillingAddress
 * @package Drupal\vib_service\Client\Model
 */
class VibUserBillingAddress implements VibObjectInterface {

  protected $address;
  protected $city;
  protected $state;
  protected $country;
  protected $postCode;

  /**
   * VibUserBillingAddress constructor.
   * @param $address
   * @param $city
   * @param $state
   * @param $country
   * @param $post_code
   */
  public function __construct($address, $city, $state, $country, $post_code) {
    $this->address = $address;
    $this->city = $city;
    $this->state = $state;
    $this->country = $country;
    $this->postCode = $post_code;
  }

  /**
   * @return string
   */
  public function getAddress() {
    return $this->address;
  }

  /**
   * @return string
   */
  public function getCity() {
    return $this->city;
  }

  /**
   * @return string
   */
  public function getState() {
    return $this->state;
  }

  /**
   * @return string
   */
  public function getCountry() {
    return $this->country;
  }

  /**
   * @return string
   */
  public function getPostCode() {
    return $this->postCode;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [];
  }

  /**
   * @param $user_info
   */
  public function alterOpenIdUserInfo(&$user_info){
    // These keys have to match the keys of the claims.
    $user_info['vib_billing_address_address'] = $this->getAddress();
    $user_info['vib_billing_address_city'] = $this->getCity();
    $user_info['vib_billing_address_state'] = $this->getState();
    $user_info['vib_billing_address_country'] = $this->getCountry();
    $user_info['vib_billing_address_post_code'] = $this->getPostCode();
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static(
      $json['Address'],
      $json['City'],
      $json['State'],
      $json['Country'],
      $json['PostCode']
    );
  }

  /**
   * The claims available in VIB Service for billing address.
   *
   * @param $claims
   */
  public static function claims(&$claims) {
    $claims['vib_billing_address_address'] = [
      'scope' => 'profile',
      'title' => 'Address',
      'type' => 'string',
    ];

    $claims['vib_billing_address_city'] = [
      'scope' => 'profile',
      'title' => 'City',
      'type' => 'string',
    ];

    $claims['vib_billing_address_state'] = [
      'scope' => 'profile',
      'title' => 'State',
      'type' => 'string',
    ];

    $claims['vib_billing_address_country'] = [
      'scope' => 'profile',
      'title' => 'Country',
      'type' => 'string',
    ];

    $claims['vib_billing_address_post_code'] = [
      'scope' => 'profile',
      'title' => 'Post code',
      'type' => 'string',
    ];
  }

}