<?php

namespace Drupal\vib_service\Client\Model\Request;

use Drupal\vib_service\Client\Model\VibCentre;
use Drupal\vib_service\Client\Model\VibObjectInterface;
use Drupal\vib_service\Client\Model\VibRole;

/**
 * Class VibUserListRequest
 * @package Drupal\vib_service\Client\Model\Request
 */
class VibUserListRequest implements VibObjectInterface {

  protected $page;
  protected $pageSize;
  protected $searchTerm;
  protected $sortOrder;
  protected $sortField;
  protected $userIds;
  protected $role;
  protected $pi;
  protected $centre;
  protected $fetchAll;

  /**
   * VibUserListRequest constructor.
   * @param $page
   * @param $pageSize
   * @param string $searchTerm
   * @param string $sortOrder
   * @param string $sortField
   * @param array $userIds
   * @param \Drupal\vib_service\Client\Model\VibRole|NULL $role
   * @param bool $pi
   * @param \Drupal\vib_service\Client\Model\VibCentre|NULL $centre
   * @param bool $fetch_all
   */
  public function __construct($page, $pageSize, $searchTerm = '', $sortOrder = 'asc', $sortField = 'Name', $userIds = [], VibRole $role = NULL, $pi = FALSE, VibCentre $centre = NULL, $fetch_all = FALSE) {
    $this->page = $page;
    $this->pageSize = $pageSize;
    $this->searchTerm = $searchTerm;
    $this->sortOrder = $sortOrder;
    $this->sortField = $sortField;
    $this->userIds = $userIds;
    $this->role = $role;
    $this->pi = $pi;
    $this->centre = $centre;
    $this->fetchAll = $fetch_all;
  }

  /**
   * @return integer
   */
  public function getPage() {
    return $this->page;
  }

  /**
   * @param $page
   */
  public function setPage($page) {
    $this->page = $page;
  }

  /**
   * @return $this
   */
  public function nextPage() {
    $this->page++;
    return $this;
  }

  /**
   * @return integer
   */
  public function getPageSize() {
    return $this->pageSize;
  }

  /**
   * @return string
   */
  public function getSearchTerm() {
    return $this->searchTerm;
  }

  /**
   * @return string
   */
  public function getSortOrder() {
    return $this->sortOrder;
  }

  /**
   * @return string
   */
  public function getSortField() {
    return $this->sortField;
  }

  /**
   * @return array
   */
  public function getUserIds() {
    return $this->userIds;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibRole|NULL
   */
  public function getRole() {
    return $this->role;
  }

  /**
   * @return bool
   */
  public function isPi() {
    return $this->pi;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibCentre|NULL
   */
  public function getCentre() {
    return $this->centre;
  }

  /**
   * @return bool
   */
  public function fetchAll(){
    return $this->fetchAll;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'page' => $this->getPage(),
      'pageSize' => $this->getPageSize(),
      'searchTerm' => $this->getSearchTerm(),
      'sortOrder' => $this->getSortOrder(),
      'sortField' => $this->getSortField(),
      'userIds' => $this->getUserIds(),
      'role' => $this->getRole() ? $this->getRole()->getName() : NULL,
      'pi' => $this->isPi(),
      'center' => $this->getCentre() ? $this->getCentre()->getCode() : NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return [];
  }

}
