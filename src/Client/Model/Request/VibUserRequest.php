<?php

namespace Drupal\vib_service\Client\Model\Request;

use Drupal\vib_service\Client\Model\VibMetaData;
use Drupal\vib_service\Client\Model\VibObjectInterface;
use Drupal\vib_service\Client\Model\VibUserVibInfo;

/**
 * Class VibUserRequest
 * @package Drupal\vib_service\Client\Model\Request
 */
class VibUserRequest implements VibObjectInterface {

  protected $title;
  protected $firstName;
  protected $lastName;
  protected $email;
  protected $phone;
  protected $linkedInUrl;
  protected $company;
  protected $password;
  protected $vibInfo;
  protected $metaData;

  /**
   * VibUserRequest constructor.
   * @param $title
   * @param $first_name
   * @param $last_name
   * @param $email
   * @param $phone
   * @param $linked_in_url
   * @param $company
   * @param $password
   * @param \Drupal\vib_service\Client\Model\VibUserVibInfo $vib_info
   * @param \Drupal\vib_service\Client\Model\VibMetaData $data
   */
  public function __construct($title, $first_name, $last_name, $email, $phone, $linked_in_url, $company, $password, VibUserVibInfo $vib_info, VibMetaData $data) {
    /* TODO: Do we need to add the password here? */
    $this->title = $title;
    $this->firstName = $first_name;
    $this->lastName = $last_name;
    $this->email = $email;
    $this->phone = $phone;
    $this->linkedInUrl = $linked_in_url;
    $this->company = $company;
    $this->vibInfo = $vib_info;
    $this->metaData = $data;
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @return string
   */
  public function getFirstName() {
    return $this->firstName;
  }

  /**
   * @return string
   */
  public function getLastName() {
    return $this->lastName;
  }

  /**
   * @return mixed
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * @return string
   */
  public function getPhone() {
    return $this->phone;
  }

  /**
   * @return string
   */
  public function getLinkedInUrl() {
    return $this->linkedInUrl;
  }

  /**
   * @return string
   */
  public function getCompany() {
    return $this->company;
  }

  /**
   * @return string
   */
  public function getPassword() {
    return $this->password;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibUserVibInfo
   */
  public function getVibInfo() {
    return $this->vibInfo;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibMetaData
   */
  public function getMetaData() {
    return $this->metaData;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'Title' => $this->getTitle(),
      'FirstName' => $this->getFirstName(),
      'LastName' => $this->getLastName(),
      'Email' => $this->getEmail(),
      'Company' => $this->getCompany(),
      'Phone' => $this->getPhone(),
      'LinkedInUrl' => $this->getLinkedInUrl(),
      'Alumni' => $this->getVibInfo()->isAlumni(),
      'AlumniCenter' => $this->getVibInfo()->getAlumniCentre(),
      'Metadata' => $this->getMetaData()->toJson(),
      'Password' => $this->getPassword(),
      'ProfilePicId' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return [];
  }
}
