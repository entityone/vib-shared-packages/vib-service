<?php

namespace Drupal\vib_service\Client\Model;

use Drupal\vib_service\Client\Model\Request\VibUserRequest as VibUserRequestBase;

/**
 * Class VibUserRequest
 * @package Drupal\vib_service\Client\Model
 * @deprecated
 */
class VibUserRequest extends VibUserRequestBase {

}
