<?php

namespace Drupal\vib_service\Client\Model;

/**
 * Class VibMetaData
 * @package Drupal\vib_service\Client\Model
 */
class VibMetaData implements VibObjectInterface {

  protected $data;

  /**
   * VibMetaData constructor.
   * @param array $data
   */
  public function __construct(array $data) {
    $this->data = $data;
  }

  /**
   * @param $key
   * @param bool $default
   * @return bool|mixed
   */
  public function get($key, $default = FALSE) {
    if (isset($this->data[$key])) {
      return $this->data[$key];
    }

    return $default;
  }

  /**
   * @param array $data
   */
  public function setData(array $data) {
    $this->data = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return $this->data ? $this->data : new \stdClass();
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static($json);
  }


}