<?php

namespace Drupal\vib_service\Client\Model;

/**
 * Class VibFunction
 * @package Drupal\vib_service\Client\Model
 */
class VibFunction implements VibObjectInterface {

  protected $label;

  /**
   * VibFunction constructor.
   * @param $label
   */
  public function __construct($label) {
    $this->label = $label;
  }

  /**
   * @return string
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static($json['Label']);
  }

}
