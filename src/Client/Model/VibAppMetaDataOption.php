<?php


namespace Drupal\vib_service\Client\Model;

/**
 * Class VibAppMetaDataOption
 * @package Drupal\vib_service\Client\Model
 */
class VibAppMetaDataOption implements VibObjectInterface {

  protected $id;
  protected $text;

  /**
   * VibAppMetaDataOption constructor.
   * @param $id
   * @param $text
   */
  public function __construct($id, $text) {
    $this->id = $id;
    $this->text = $text;
  }

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getText() {
    return $this->text;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'id' => $this->getId(),
      'text' => $this->getText(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static($json['id'], $json['text']);
  }


}
