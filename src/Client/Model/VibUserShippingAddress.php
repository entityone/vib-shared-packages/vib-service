<?php

namespace Drupal\vib_service\Client\Model;

use phpDocumentor\Reflection\Types\Static_;

/**
 * Class VibUserShippingAddress
 * @package Drupal\vib_service\Client\Model
 */
class VibUserShippingAddress implements VibObjectInterface {

  protected $address;
  protected $city;
  protected $state;
  protected $country;
  protected $countryName;
  protected $postCode;

  /**
   * VibUserShippingAddress constructor.
   * @param $address
   * @param $city
   * @param $state
   * @param $country
   * @param $countryName
   * @param $postCode
   */
  public function __construct($address, $city, $state, $country, $country_name, $post_code) {
    $this->address = $address;
    $this->city = $city;
    $this->state = $state;
    $this->country = $country;
    $this->countryName = $country_name;
    $this->postCode = $post_code;
  }

  /**
   * @return string
   */
  public function getAddress() {
    return $this->address;
  }

  /**
   * @return string
   */
  public function getCity() {
    return $this->city;
  }

  /**
   * @return string
   */
  public function getState() {
    return $this->state;
  }

  /**
   * @return string
   */
  public function getCountry() {
    return $this->country;
  }

  /**
   * @return string
   */
  public function getCountryName() {
    return $this->countryName;
  }

  /**
   * @return string
   */
  public function getPostCode() {
    return $this->postCode;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [];
  }

  /**
   * @param $user_info
   */
  public function alterOpenIdUserInfo(&$user_info){
    // These keys have to match the keys of the claims.
    $user_info['vib_shipping_address_address'] = $this->getAddress();
    $user_info['vib_shipping_address_city'] = $this->getCity();
    $user_info['vib_shipping_address_state'] = $this->getState();
    $user_info['vib_shipping_address_country'] = $this->getCountry();
    $user_info['vib_shipping_address_country_name'] = $this->getCountryName();
    $user_info['vib_shipping_address_post_code'] = $this->getPostCode();
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static(
      $json['Address'],
      $json['City'],
      $json['State'],
      $json['Country'],
      $json['CountryName'],
      $json['PostCode']
    );
  }

  /**
   * The claims available in VIB Service for shipping address.
   *
   * @param $claims
   */
  public static function claims(&$claims) {
    $claims['vib_shipping_address_address'] = [
      'scope' => 'profile',
      'title' => 'Address',
      'type' => 'string',
    ];

    $claims['vib_shipping_address_city'] = [
      'scope' => 'profile',
      'title' => 'City',
      'type' => 'string',
    ];

    $claims['vib_shipping_address_state'] = [
      'scope' => 'profile',
      'title' => 'State',
      'type' => 'string',
    ];

    $claims['vib_shipping_address_country'] = [
      'scope' => 'profile',
      'title' => 'Country',
      'type' => 'string',
    ];

    $claims['vib_shipping_address_country_name'] = [
      'scope' => 'profile',
      'title' => 'Country name',
      'type' => 'string',
    ];

    $claims['vib_shipping_address_post_code'] = [
      'scope' => 'profile',
      'title' => 'Post code',
      'type' => 'string',
    ];
  }

}