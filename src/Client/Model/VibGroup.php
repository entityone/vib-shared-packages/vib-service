<?php

namespace Drupal\vib_service\Client\Model;

/**
 * Class VibGroup
 * @package Drupal\vib_service\Client\Model
 */
class VibGroup implements VibObjectInterface {

  protected $code;
  protected $name;

  /**
   * VibGroup constructor.
   * @param $code
   * @param $name
   */
  public function __construct($code, $name) {
    $this->code = $code;
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getCode() {
    return $this->code;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'Code' => $this->getCode(),
      'Name' => $this->getName(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static($json['Code'], $json['Name']);
  }

}
