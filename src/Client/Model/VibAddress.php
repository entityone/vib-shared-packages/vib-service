<?php

namespace Drupal\vib_service\Client\Model;

/**
 * Class VibAddress.
 *
 * @package Drupal\vib_service\Client\Model
 */
class VibAddress implements VibObjectInterface {

  protected $location;
  protected $number;
  protected $street;
  protected $additional;
  protected $floor;
  protected $city;
  protected $postcode;
  protected $state;
  protected $country;
  protected $countryCode;

  /**
   * VibAddress constructor.
   * @param string $location
   * @param string $number
   * @param string $street
   * @param string $additional
   * @param string $floor
   * @param string $city
   * @param string $postcode
   * @param string $state
   * @param string $country
   * @param string $country_code
   */
  public function __construct($location, $number, $street, $additional, $floor, $city, $postcode, $state, $country, $country_code) {
    $this->location = $location;
    $this->number = $number;
    $this->street = $street;
    $this->additional = $additional;
    $this->floor = $floor;
    $this->city = $city;
    $this->postcode = $postcode;
    $this->state = $state;
    $this->country = $country;
    $this->countryCode = $country_code;
  }

  /**
   * @return string
   */
  public function getLocation(): ?string {
    return $this->location;
  }

  /**
   * @return string
   */
  public function getNumber(): ?string {
    return $this->number;
  }

  /**
   * @return string
   */
  public function getStreet(): ?string {
    return $this->street;
  }

  /**
   * @return string
   */
  public function getAdditional(): ?string {
    return $this->additional;
  }

  /**
   * @return string
   */
  public function getFloor(): ?string {
    return $this->floor;
  }

  /**
   * @return string
   */
  public function getCity(): ?string {
    return $this->city;
  }

  /**
   * @return string
   */
  public function getPostcode(): ?string {
    return $this->postcode;
  }

  /**
   * @return string
   */
  public function getState(): ?string {
    return $this->state;
  }

  /**
   * @return string
   */
  public function getCountry(): ?string {
    return $this->country;
  }

  /**
   * @return string
   */
  public function getCountryCode(): ?string {
    return $this->countryCode;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'Location' => $this->getLocation(),
      'Number' => $this->getNumber(),
      'Street' => $this->getStreet(),
      'Additional' => $this->getAdditional(),
      'Floor' => $this->getFloor(),
      'City' => $this->getCity(),
      'Postcode' => $this->getPostcode(),
      'State' => $this->getState(),
      'Country' => $this->getCountry(),
      'CountryCode' => $this->getCountryCode(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static(
      $json['Location'] ?? '',
      $json['Number'] ?? '',
      $json['Street'] ?? '',
      $json['Additional'] ?? '',
      $json['Floor'] ?? '',
      $json['City'] ?? '',
      $json['PostCode'] ?? '',
      $json['State'] ?? '',
      $json['Country'] ?? '',
      $json['CountryCode'] ?? ''
    );
  }

}
