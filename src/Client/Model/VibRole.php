<?php

namespace Drupal\vib_service\Client\Model;

use Drupal\Core\Field\FieldFilteredMarkup;

/**
 * Class VibRole
 * @package Drupal\vib_service\Client\Model
 */
class VibRole implements VibObjectInterface, VibFieldableObjectInterface {

  protected $id;
  protected $name;
  protected $machineName;

  /**
   * VibRole constructor.
   * @param $id
   * @param $name
   */
  public function __construct($id, $name) {
    $this->id = $id;
    $this->name = $name;
    $this->machineName = self::toMachineName($this->name);
  }

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @return string
   */
  public function getMachineName() {
    return $this->machineName;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldLabel() {
    return $this->getName();
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldKey() {
    return $this->getName();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      '#markup' => $this->getName(),
      '#allowed_tags' => FieldFilteredMarkup::allowedTags(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'RoleId' => $this->getId(),
      'RoleName' => $this->getName(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static($json['RoleId'], $json['RoleName']);
  }

  /**
   * @param $name
   * @return string
   */
  public static function toMachineName($name){
    return str_replace('.', '_', $name);
  }
}
