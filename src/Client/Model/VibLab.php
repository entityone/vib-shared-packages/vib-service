<?php

namespace Drupal\vib_service\Client\Model;

/**
 * Class VibLab
 * @package Drupal\vib_service\Client\Model
 */
class VibLab implements VibObjectInterface {

  protected $code;
  protected $codeNormalized;
  protected $name;
  protected $type;
  protected $phone;
  protected $centre;
  protected $subCentre;
  protected $users;
  protected $published;
  protected $active;
  protected $core;

  /**
   * VibLab constructor.
   * @param $code
   * @param $code_normalized
   * @param $name
   * @param $type
   * @param $phone
   * @param array $users
   * @param \Drupal\vib_service\Client\Model\VibCentre|null $centre
   * @param \Drupal\vib_service\Client\Model\VibCentre|null $sub_centre
   * @param bool $published
   * @param bool $active
   * @param bool $core
   */
  public function __construct($code, $code_normalized, $name, $type, $phone, array $users, VibCentre $centre = NULL, VibCentre $sub_centre = NULL, $published = TRUE, $active = TRUE, $core = FALSE) {
    $this->code = $code;
    $this->codeNormalized = $code_normalized;
    $this->name = $name;
    $this->type = $type;
    $this->phone = $phone;
    $this->centre = $centre;
    $this->subCentre = $sub_centre;
    $this->users = $users;
    $this->published = $published;
    $this->active = $active;
    $this->core = $core;
  }

  /**
   * @return string
   */
  public function getCode() {
    return $this->code;
  }

  /**
   * @return string
   */
  public function getCodeNormalized() {
    return $this->codeNormalized;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @return string
   */
  public function getPhone() {
    return $this->phone;
  }

  /**
   * @return bool
   */
  public function isPublished() {
    return (bool) $this->published;
  }

  /**
   * @return bool
   */
  public function isActive() {
    return (bool) $this->active;
  }

  /**
   * @return bool
   */
  public function isCore() {
    return (bool) $this->core;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibCentre
   */
  public function getCentre() {
    return $this->centre;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibCentre
   */
  public function getSubCentre() {
    return $this->subCentre;
  }

  /**
   * @return array
   */
  public function getUsers() {
    return $this->users;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'Code' => $this->getCode(),
      'CodeNormalized' => $this->getCodeNormalized(),
      'Name' => $this->getName(),
      'Type' => $this->getType(),
      'Phone' => $this->getPhone(),
      'Users' => $this->getUsers(),
      'Centre' => $this->getCentre() ? $this->getCentre()->toJson() : NULL,
      'SubCentre' => $this->getSubCentre() ? $this->getSubCentre()
        ->toJson() : NULL,
      'Published' => $this->isPublished(),
      'Active' => $this->isActive(),
      'Core' => $this->isCore(),
      'Visible' => $this->isActive() && $this->isPublished(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    $centre = NULL;
    if (!empty($json['Center'])) {
      $centre = VibCentre::createFromJson($json['Center']);
    }

    $sub_centre = NULL;
    if (!empty($json['SubCenter'])) {
      $sub_centre = VibCentre::createFromJson($json['SubCenter']);
    }

    $users = $json['Users'] ?? [];
    $published = $json['Published'] ?? TRUE;
    $active = $json['Active'] ?? TRUE;
    $core = $json['Core'] ?? FALSE;
    $code_normalized = $json['CodeNormalized'] ?? NULL;
    $phone = $json['Phone'] ?? NULL;

    return new static($json['Code'], $code_normalized, $json['Name'], $json['Type'], $phone, $users, $centre, $sub_centre, $published, $active, $core);
  }

}
