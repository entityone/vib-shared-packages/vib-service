<?php

namespace Drupal\vib_service\Client\Model;

/**
 * Interface VibFieldableObjectInterface
 * @package Drupal\vib_service\Client\Model
 */
interface VibFieldableObjectInterface {

  /**
   * @return string
   */
  public function getFieldLabel();

  /**
   * @return string
   */
  public function getFieldKey();

  /**
   * @return array
   */
  public function render();

}