<?php

namespace Drupal\vib_service\Client\Model;

/**
 * Class VibUserVibInfo
 * @package Drupal\vib_service\Client\Model
 */
class VibUserVibInfo {

  protected $vibId;
  protected $centres;
  protected $subCentres;
  protected $labs;
  protected $functionEN;
  protected $functionNL;
  protected $isAlumni;
  protected $isLabOwner;
  protected $groups;
  protected $alumniCentre;
  protected $metaFunctionGroup;

  /**
   * VibUserVibInfo constructor.
   * @param $vibId
   * @param array $centres
   * @param array $sub_centres
   * @param array $labs
   * @param $function_en
   * @param $function_nl
   * @param $is_alumni
   * @param $is_lab_owner
   * @param array $groups
   * @param $meta_function
   * @param \Drupal\vib_service\Client\Model\VibCentre|null $alumni_centre
   */
  public function __construct($vibId, array $centres, array $sub_centres, array $labs, $function_en, $function_nl, $is_alumni, $is_lab_owner, array $groups, $meta_function_group, VibCentre $alumni_centre = NULL) {
    $this->vibId = $vibId;
    $this->centres = $centres;
    $this->subCentres = $sub_centres;
    $this->labs = $labs;
    $this->functionEN = $function_en;
    $this->functionNL = $function_nl;
    $this->isAlumni = $is_alumni;
    $this->alumniCentre = $alumni_centre;
    $this->isLabOwner = $is_lab_owner;
    $this->groups = $groups;
    $this->metaFunctionGroup = $meta_function_group;
  }

  /**
   * @return string
   */
  public function getVibId() {
    return $this->vibId;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibCentre[]
   */
  public function getCentres() {
    return $this->centres;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibCentre[]
   */
  public function getSubCentres() {
    return $this->subCentres;
  }

  /**
   * @return array
   */
  public function getLabs() {
    return $this->labs;
  }

  /**
   * @return string
   */
  public function getFunctionEN() {
    return $this->functionEN;
  }

  /**
   * @return string
   */
  public function getFunctionNL() {
    return $this->functionNL;
  }

  /**
   * @return bool
   */
  public function isAlumni() {
    return $this->isAlumni;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibCentre|NULL
   */
  public function getAlumniCentre() {
    return $this->alumniCentre;
  }

  /**
   * @return bool
   */
  public function isLabOwner() {
    return $this->isLabOwner;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibGroup[]
   */
  public function getGroups() {
    return $this->groups;
  }

  /**
   * @return string
   */
  public function getMetaFunctionGroup() {
    return $this->metaFunctionGroup;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'VIBId' => $this->getVibId(),
      'Centers' => array_map(function (VibCentre $centre) {
        return $centre->toJson();
      }, $this->getCentres()),
      'SubCenters' => array_map(function (VibCentre $centre) {
        return $centre->toJson();
      }, $this->getSubCentres()),
      'Labs' => $this->getLabs(),
      'FunctionEN' => $this->getFunctionEN(),
      'FunctionNL' => $this->getFunctionNL(),
      'Alumni' => $this->isAlumni(),
      'LabOwner' => $this->isLabOwner(),
      'Groups' => array_map(function (VibGroup $group) {
        return $group->toJson();
      }, $this->getGroups()),
      'AlumniCenter' => $this->getAlumniCentre() ? $this->getAlumniCentre()
        ->toJson() : NULL,
      'MetaFunctionGroup' => $this->getMetaFunctionGroup(),
    ];
  }

  /**
   * @param $user_info
   */
  public function alterOpenIdUserInfo(&$user_info) {
    // These keys have to match the keys of the claims.
    $user_info['vib_info_function_en'] = $this->getFunctionEN();
    $user_info['vib_info_function_nl'] = $this->getFunctionNL();
    $user_info['vib_info_centres'] = array_map(function (VibCentre $centre) {
      return $centre->getCode();
    }, $this->getCentres());
    $user_info['vib_info_labs'] = $this->getLabs();
    $user_info['vib_info_alumni'] = $this->isAlumni();
    $user_info['vib_info_alumni_centre'] = $this->getAlumniCentre() ? $this->getAlumniCentre()
      ->getCode() : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    $centres = [];
    foreach ($json['Centers'] as $centre) {
      $centres[] = VibCentre::createFromJson($centre);
    }

    $sub_centres = [];
    if (!empty($json['SubCenters'])) {
      foreach ($json['SubCenters'] as $centre) {
        $sub_centres[] = VibCentre::createFromJson($centre);
      }
    }

    $alumni_centre = NULL;
    if (!empty($json['AlumniCenter'])) {
      $alumni_centre = VibCentre::createFromJson(['CenterCode' => $json['AlumniCenter']]);
    }

    $groups = [];
    if (!empty($json['Groups'])) {
      foreach ($json['Groups'] as $group) {
        $groups[] = VibGroup::createFromJson($group);
      }
    }

    return new static(
      $json['VIBId'] ?? NULL,
      $centres,
      $sub_centres,
      $json['Labs'],
      $json['FunctionEN'],
      $json['FunctionNL'],
      $json['Alumni'],
      $json['LabOwner'] ?? FALSE,
      $groups,
      $json['MetaFunctionGroup'] ?? NULL,
      $alumni_centre
    );
  }

  /**
   * The claims available in VIB Service for VIB info.
   *
   * @param $claims
   */
  public static function claims(&$claims) {
    $claims['vib_info_function_en'] = [
      'scope' => 'profile',
      'title' => 'Function EN',
      'type' => 'string',
    ];

    $claims['vib_info_function_nl'] = [
      'scope' => 'profile',
      'title' => 'Function NL',
      'type' => 'string',
    ];

    $claims['vib_info_centres'] = [
      'scope' => 'profile',
      'title' => 'Centres',
      'type' => 'string',
    ];

    $claims['vib_info_labs'] = [
      'scope' => 'profile',
      'title' => 'Labs',
      'type' => 'string',
    ];

    $claims['vib_info_alumni'] = [
      'scope' => 'profile',
      'title' => 'Is VIB alumni',
      'type' => 'string',
    ];

    $claims['vib_info_alumni_centre'] = [
      'scope' => 'profile',
      'title' => 'Alumni centre',
      'type' => 'string',
    ];
  }

}
