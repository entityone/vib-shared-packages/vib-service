<?php

namespace Drupal\vib_service\Client\Model;

/**
 * Class VibJobAllocation.
 *
 * @package Drupal\vib_service\Client\Model
 */
class VibJobAllocation implements VibObjectInterface {

  protected $jobAllocationCode;
  protected $jobAllocationName;
  protected $centerCode;
  protected $centerName;
  protected $projectCode;
  protected $projectName;

  /**
   * VibJobAllocation constructor.
   * @param string $job_allocation_code
   * @param string $job_allocation_name
   * @param string $centerCode
   * @param string $centerName
   * @param string $projectCode
   * @param string $projectName
   */
  public function __construct($job_allocation_code, $job_allocation_name, $centerCode, $centerName, $projectCode, $projectName) {
    $this->jobAllocationCode = $job_allocation_code;
    $this->jobAllocationName = $job_allocation_name;
    $this->centerCode = $centerCode;
    $this->centerName = $centerName;
    $this->projectCode = $projectCode;
    $this->projectName = $projectName;
  }

  /**
   * @return string
   */
  public function getCode(): string {
    return $this->jobAllocationCode;
  }

  /**
   * @return string
   */
  public function getName(): string {
    return $this->jobAllocationName;
  }

  /**
   * @return string
   */
  public function getCenterCode(): string {
    return $this->centerCode;
  }

  /**
   * @return string
   */
  public function getCenterName(): string {
    return $this->centerName;
  }

  /**
   * @return string
   */
  public function getProjectCode(): string {
    return $this->projectCode;
  }

  /**
   * @return string
   */
  public function getProjectName(): string {
    return $this->projectName;
  }


  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'JobAllocationName' => $this->getName(),
      'JobAllocationCode' => $this->getCode(),
      'ProjectName' => $this->getProjectName(),
      'ProjectCode' => $this->getProjectCode(),
      'CenterName' => $this->getCenterName(),
      'CenterCode' => $this->getCenterCode(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return new static(
      $json['JobAllocationCode'],
      $json['JobAllocationName'],
      $json['CenterCode'],
      $json['CenterName'],
      $json['ProjectCode'],
      $json['ProjectName']
    );
  }


}
