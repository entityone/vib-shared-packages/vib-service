<?php

namespace Drupal\vib_service\Client\Model;

/**
 * Class VibCentre
 * @package Drupal\vib_service\Client\Model
 */
class VibCentre implements VibObjectInterface {

  protected $code;
  protected $description;
  protected $phone;
  protected $published;
  protected $subCentres;
  protected $associatedCentres;
  protected $associatedCores;
  protected $address;

  /**
   * VibCentre constructor.
   * @param $code
   * @param $description
   * @param $phone
   * @param $published
   * @param array $sub_centres
   * @param array $associated_centres
   * @param array $associated_cores
   * @param \Drupal\vib_service\Client\Model\VibAddress $address
   */
  public function __construct($code, $description, $phone, $published, array $sub_centres, array $associated_centres, array $associated_cores, VibAddress $address) {
    $this->code = $code;
    $this->description = $description;
    $this->phone = $phone;
    $this->published = $published;
    $this->subCentres = $sub_centres;
    $this->associatedCentres = $associated_centres;
    $this->associatedCores = $associated_cores;
    $this->address = $address;
  }

  /**
   * @return string
   */
  public function getCode() {
    return $this->code;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @return string
   */
  public function getPhone() {
    return $this->phone;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibCentre[]
   */
  public function getSubCentres() {
    return $this->subCentres;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibCentre[]
   */
  public function getAssociatedCentres() {
    return $this->associatedCentres;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibCentre[]
   */
  public function getAssociatedCores() {
    return $this->associatedCores;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibAddress
   */
  public function getAddress() {
    return $this->address;
  }

  /**
   * @return bool
   */
  public function isPublished() {
    return (bool) $this->published;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'CenterCode' => $this->getCode(),
      'Description' => $this->getDescription(),
      'Phone' => $this->getPhone(),
      'Published' => $this->isPublished(),
      'Address' => $this->getAddress() ? $this->getAddress()->toJson() : [],
      'SubCentres' => array_map(function (VibCentre $centre) {
        return $centre->toJson();
      }, $this->getSubCentres()),
      'AssociatedCenters' => array_map(function (VibCentre $centre) {
        return $centre->toJson();
      }, $this->getAssociatedCentres()),
      'AssociatedCores' => array_map(function (VibCentre $centre) {
        return $centre->toJson();
      }, $this->getAssociatedCores()),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    $sub_centres = [];
    $associated_centres = [];
    $associated_cores = [];

    if (!empty($json['SubCenters'])) {
      foreach ($json['SubCenters'] as $centre) {
        $sub_centres[] = VibCentre::createFromJson($centre);
      }
    }

    if (!empty($json['AssociatedCenters'])) {
      foreach ($json['AssociatedCenters'] as $centre) {
        $associated_centres[] = VibCentre::createFromJson($centre);
      }
    }

    if (!empty($json['AssociatedCores'])) {
      foreach ($json['AssociatedCores'] as $centre) {
        $associated_cores[] = VibCentre::createFromJson($centre);
      }
    }

    return new static(
      $json['CenterCode'],
      $json['Description'],
      $json['Phone'] ?? NULL,
      $json['Published'] ?? TRUE,
      $sub_centres,
      $associated_centres,
      $associated_cores,
      VibAddress::createFromJson($json['Address'] ?? [])
    );
  }

}
