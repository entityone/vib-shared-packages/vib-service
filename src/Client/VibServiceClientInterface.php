<?php

namespace Drupal\vib_service\Client;

use Drupal\vib_service\Client\Model\Request\VibUserListRequest;
use Drupal\vib_service\Client\Model\Request\VibUserRequest;

/**
 * Interface VibServiceClientInterface
 * @package Drupal\vib_service\Client
 */
interface VibServiceClientInterface {

  /**
   * @return \Drupal\vib_service\Client\Model\VibRole[]
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function apiGetRoles(bool $cached = TRUE);

  /**
   * @return \Drupal\vib_service\Client\Model\VibTeam[]
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function apiGetTeams(bool $cached = TRUE);

  /**
   * @param \Drupal\vib_service\Client\Model\Request\VibUserListRequest $request
   * @return \Drupal\vib_service\Client\Model\VibUserResponse[]
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function apiGetUsers(VibUserListRequest $request);

  /**
   * @param $id
   * @return \Drupal\vib_service\Client\Model\VibUserResponse
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function apiGetUser($id);

  /**
   * @param $email
   * @return \Drupal\vib_service\Client\Model\VibUserResponse
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function apiGetUserByEmail($email);

  /**
   * @param $id
   * @param $cropped
   * @return string
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function apiGetProfilePicture($id, $cropped = FALSE);

  /**
   * @param \Drupal\vib_service\Client\Model\Request\VibUserRequest $request
   * @return \Drupal\vib_service\Client\Model\VibUserResponse
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function apiCreateUser(VibUserRequest $request);

  /**
   * @param $id
   * @param $password
   * @param $activation_token
   * @return boolean|\Drupal\vib_service\Client\Model\VibUserResponse
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function apiActivateUser($id, $activation_token, $password);

  /**
   * @return \Drupal\vib_service\Client\Model\VibAppMetaData[]
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function apiGetAppMetadata();

  /**
   * @return \Drupal\vib_service\Client\Model\VibCentre[]
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function apiGetCentres();

  /**
   * @return \Drupal\vib_service\Client\Model\VibLab[]
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function apiGetLabs();

  /**
   * @return \Drupal\vib_service\Client\Model\VibFunction[]
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function apiGetFunctions();

  /**
   * @return \Drupal\vib_service\Client\Model\VibProject[]
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function apiGetProjects();

  /**
   * @return \Drupal\vib_service\Client\Model\VibJobAllocation[]
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function apiGetJobAllocations();
}
