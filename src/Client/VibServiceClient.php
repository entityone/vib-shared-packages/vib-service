<?php

namespace Drupal\vib_service\Client;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\State\StateInterface;
use Drupal\vib_service\Client\Model\Request\VibUserListRequest;
use Drupal\vib_service\Client\Model\Request\VibUserRequest;
use Drupal\vib_service\Client\Model\VibAppMetaData;
use Drupal\vib_service\Client\Model\VibCentre;
use Drupal\vib_service\Client\Model\VibFunction;
use Drupal\vib_service\Client\Model\VibJobAllocation;
use Drupal\vib_service\Client\Model\VibLab;
use Drupal\vib_service\Client\Model\VibProject;
use Drupal\vib_service\Client\Model\VibRole;
use Drupal\vib_service\Client\Model\VibTeam;
use Drupal\vib_service\Client\Model\VibUserResponse;
use GuzzleHttp\RequestOptions;

/**
 * Interface VibServiceClientInterface
 * @package Drupal\vib_service\Client
 */
class VibServiceClient implements VibServiceClientInterface {

  const STATE_BEARER_TOKEN = 'vib_services_bearer_token';
  const CACHE_OFFSET = 60 * 60 * 24 * 31;

  protected $config;
  protected $clientFactory;
  protected $httpClient;
  protected $state;

  /**
   * The cache bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * VibServiceClient constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   * @param \Drupal\Core\State\StateInterface $state
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache bin.
   */
  function __construct(ConfigFactoryInterface $config_factory, ClientFactory $http_client_factory, StateInterface $state, CacheBackendInterface $cache) {
    $this->config = $config_factory->get('vib_service.settings');
    $this->clientFactory = $http_client_factory;
    $this->state = $state;
    $this->httpClient = $this->createFromOptions();
    $this->cache = $cache;
  }

  /**
   * @return array|\GuzzleHttp\Client
   */
  protected function createFromOptions() {
    $client = drupal_static(__FUNCTION__);

    if (!isset($client)) {
      $endpoint = $this->config->get('api_endpoint');
      $options = [
        'base_uri' => substr($endpoint, -1, 1) == '/' ? $endpoint : $endpoint . '/',
        'headers' => [
          'Content-Type' => 'application/json',
        ]
      ];

      $client = $this->clientFactory->fromOptions($options);
    }

    return $client;
  }

  /**
   * Retrieves the configured API cache time.
   *
   * @return int
   */
  protected function getCacheTime(): int {
    return (int) ($this->config->get('cache_time') ?? 604800);
  }

  /**
   * Refreshes bearer token.
   *
   * @param string $scope
   *   The scope.
   *
   * @return string
   *   The bearer token.
   *
   * @throws \Drupal\vib_service\Client\VibClientException
   */
  protected function apiRefreshBearerToken(string $scope) {
    try {
      $request_options = [
        'form_params' => [
          'client_id' => $this->config->get('client_id'),
          'client_secret' => $this->config->get('client_secret'),
          'grant_type' => 'client_credentials',
          'scope' => $scope,
        ],
      ];

      $response = $this->clientFactory->fromOptions()
        ->request('POST', $this->config->get('token_endpoint'), $request_options);
      $json = Json::decode($response->getBody()->getContents());

      if (empty($json['access_token'])) {
        throw new VibClientException('Could not API fetch bearer token');
      }

      return $json['access_token'];
    } catch (\Exception $e) {
      throw new VibClientException($e->getMessage());
    }
  }

  /**
   * Returns (cached) bearer token.
   *
   * @param string $scope
   *   The scope.
   *
   * @return string
   *   The bearer token.
   *
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function apiBearerToken($scope = 'orders roles teams users codes') {
    if (!$tokens = $this->state->get(self::STATE_BEARER_TOKEN)) {
      $token = $this->apiRefreshBearerToken($scope);
      // Cache token per scope.
      $tokens[$scope] = $token;
      $this->state->set(self::STATE_BEARER_TOKEN, $tokens);
    }

    if (empty($tokens[$scope])) {
      $token = $this->apiRefreshBearerToken($scope);
      // Cache token per scope.
      $tokens[$scope] = $token;
      $this->state->set(self::STATE_BEARER_TOKEN, $tokens);
    }

    return $tokens[$scope];
  }

  /**
   * @param $method
   * @param $path
   * @param array $options
   * @param bool $return_as_json
   * @return array|\GuzzleHttp\Psr7\Response
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function request($method, $path, $options = [], $return_as_json = TRUE) {
    $cache_id = $path;
    $cache = &drupal_static(__FUNCTION__);

    if ($method === 'GET' && isset($cache[$cache_id])) {
      return $cache[$path];
    }

    // Keep track of loops to make sure we do not stay here forever.
    // This can occur when VIB services would be "down".
    if (!$loops = &drupal_static(__FUNCTION__ . 'loops')) {
      $loops = 1;
    }
    try {
      // Add Bearer token.
      $options['headers']['Authorization'] = 'Bearer ' . $this->apiBearerToken();

      /** @var \GuzzleHttp\Psr7\Response $response */
      $response = $this->httpClient->request($method, $path, $options);
    } catch (\Exception $e) {
      if ($e->getCode() === 401 && $loops < 3) {
        // Bearer token is probably expired.
        // Delete expired one and execute request again.
        $this->state->delete(self::STATE_BEARER_TOKEN);
        $loops++;
        return $this->request($method, $path, $options, $return_as_json);
      }

      throw new VibClientException($e->getMessage());
    }

    if ($response->getStatusCode() == 200 || $response->getStatusCode() == 201) {
      if ($return_as_json) {
        // Cache response and return it.
        $cache[$cache_id] = Json::decode($response->getBody()->getContents());
        return $cache[$cache_id];
      }
      return $response;
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function apiGetRoles(bool $cached = TRUE) {
    $cache_key = 'vib_service.client.roles';

    // Refresh cache if needed.
    if (!$cached || (!$cache = $this->cache->get($cache_key))) {
      $roles = [];

      $json = $this->request('GET', 'roles');
      foreach ($json as $role) {
        $roles[] = VibRole::createFromJson($role);
      }

      // Cache roles.
      $this->cache->set($cache_key, $roles, time() + $this->getCacheTime());
      $cache = new \stdClass();
      $cache->data = $roles;
    }

    return $cache->data;
  }

  /**
   * {@inheritdoc}
   */
  public function apiGetTeams(bool $cached = TRUE) {
    $cache_key = 'vib_service.client.teams';

    // Refresh cache if needed.
    if (!$cached || (!$cache = $this->cache->get($cache_key))) {
      $teams = [];

      $page = 1;
      $page_size = 25;
      $json = $this->request('GET', 'teams/list?page=' . $page . '&pageSize=' . $page_size);

      while ($page <= ceil($json['TotalCount'] / $page_size)) {
        foreach ($json['Data'] as $team) {
          if (count($teams) === $json['TotalCount']) {
            break;
          }
          $teams[$team['Id']] = $team;
        }

        $page++;
        $json = $this->request('GET', 'teams/list?page=' . $page . '&pageSize=' . $page_size);
      }

      // Cache teams.
      $this->cache->set($cache_key, $teams, time() + $this->getCacheTime());
      $cache = new \stdClass();
      $cache->data = $teams;
    }

    return array_map(function($team) {
      return VibTeam::createFromJson($team);
    }, $cache->data);
  }

  /**
   * {@inheritdoc}
   */
  public function apiGetUsers(VibUserListRequest $request) {
    $users = [];

    $json = $this->request('POST', 'users/list', [RequestOptions::JSON => $request->toJson()]);

    if (empty($json['Data'])) {
      // Shortcut.
      return $users;
    }

    foreach ($json['Data'] as $user) {
      $this->addTeams($user);
      $users[] = VibUserResponse::createFromJson($user);
    }

    if ($request->fetchAll() && !empty($json['TotalCount'])) {
      // Fetch all remaining pages.
      if ($total_pages = ceil($json['TotalCount'] / $request->getPageSize())) {
        // Paging starts counting at 1, and we already fetched the first page.
        // Start fetching from page 2.
        for ($i = 2; $i <= $total_pages; $i++) {
          $request->nextPage();
          $json = $this->request('POST', 'users/list', [RequestOptions::JSON => $request->toJson()]);

          if (empty($json['Data'])) {
            break;
          }

          foreach ($json['Data'] as $user) {
            $this->addTeams($user);
            $users[] = VibUserResponse::createFromJson($user);
          }
        }
      }
    }

    return $users;
  }

  /**
   * {@inheritdoc}
   */
  public function apiGetUser($id) {
    $json = $this->request('GET', 'users/' . $id);

    $this->addTeams($json);
    return VibUserResponse::createFromJson($json);
  }

  /**
   * {@inheritdoc}
   */
  public function apiGetUserByEmail($email) {
    $user = NULL;

    try {
      $options = [
        RequestOptions::HTTP_ERRORS => FALSE,
        // Don't throw exception on 404 not found.
      ];
      if ($json = $this->request('GET', 'users?email=' . $email, $options)) {
        $this->addTeams($json);
        $user = VibUserResponse::createFromJson($json);
      }
    } catch (\Exception $e) {
      if ($e->getCode() == 404) {
        // User was not found, just return FALSE.
        return NULL;
      }
      throw new VibClientException($e->getMessage());
    }

    return $user;
  }

  /**
   * {@inheritdoc}
   */
  public function apiGetProfilePicture($id, $cropped = FALSE) {
    $path = 'users/' . $id . '/profilepic';
    if ($cropped) {
      $path .= '?mode=crop';
    }

    return $this->request('POST', $path);
  }

  /**
   * {@inheritdoc}
   */
  public function apiCreateUser(VibUserRequest $request) {
    $user = NULL;

    try {
      $options = [
        RequestOptions::JSON => $request->toJson(),
      ];

      $json = $this->request('POST', 'users', $options);

      if (empty($json['Success'])) {
        $message = NULL;
        if (count($json['Errors']) == 1 && in_array('V1065', $json['Errors'])) {
          // Tried to create a user account with an already existing email.
          // Just return FALSE without throwing exception.
          return $user;
        }
        foreach ($json['Errors'] as $error_code) {
          $message .= $error_code . ', ';
        }
        throw new VibClientException($message);
      }

      $this->addTeams($json['Data']);
      $user = VibUserResponse::createFromJson($json['Data']);
    } catch (\Exception $e) {
      throw new VibClientException($e->getMessage());
    }

    return $user;
  }

  /**
   * {@inheritdoc}
   */
  public function apiActivateUser($id, $activation_token, $password) {
    try {
      $options = [
        RequestOptions::JSON => [
          'ActivationToken' => $activation_token,
          'Password' => $password,
        ],
      ];

      $json = $this->request('POST', 'users/' . $id . '/activate', $options);

      if (empty($json['Success'])) {
        return FALSE;
      }

      $this->addTeams($json['Data']);
      $user = VibUserResponse::createFromJson($json['Data']);
    } catch (\Exception $e) {
      throw new VibClientException($e->getMessage());
    }

    return $user;
  }

  /**
   * {@inheritdoc}
   */
  public function apiGetAppMetadata() {
    $metadata = [];

    $json = $this->request('GET', 'apps/metadata');

    foreach ($json as $entry) {
      $metadata[] = VibAppMetaData::createFromJson($entry);
    }

    return $metadata;
  }

  /**
   * {@inheritdoc}
   */
  public function apiGetCentres() {
    $centres = [];

    $json = $this->request('GET', 'centers');

    foreach ($json as $entry) {
      $centres[] = VibCentre::createFromJson($entry);
    }

    return $centres;
  }

  /**
   * {@inheritdoc}
   */
  public function apiGetLabs() {
    $labs = [];

    $json = $this->request('GET', 'labs');

    foreach ($json as $entry) {
      $labs[] = VibLab::createFromJson($entry);
    }

    return $labs;
  }

  /**
   * {@inheritdoc}
   */
  public function apiGetFunctions() {
    $functions = [];

    $json = $this->request('GET', 'functions');

    foreach ($json as $entry) {
      $functions[] = VibFunction::createFromJson(['Label' => $entry]);
    }

    return $functions;
  }

  /**
   * {@inheritdoc}
   */
  public function apiGetProjects() {
    $projects = [];

    $json = $this->request('GET', 'projects');

    foreach ($json as $entry) {
      $projects[] = VibProject::createFromJson($entry);
    }

    return $projects;
  }

  /**
   * {@inheritdoc}
   */
  public function apiGetJobAllocations() {
    $job_allocation = [];

    $json = $this->request('GET', 'jobs');

    foreach ($json as $entry) {
      $job_allocation[] = VibJobAllocation::createFromJson($entry);
    }

    return $job_allocation;
  }

  /**
   * Adds the teams to the user object.
   *
   * @param array $user
   *   The user json object.
   *
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function addTeams(array &$user) {
    if ($teams = $this->request('GET', 'teams?userId=' . $user['Id'])) {
      $user['Teams'] = $teams;
    }
  }

}
