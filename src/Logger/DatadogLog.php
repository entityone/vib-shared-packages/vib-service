<?php

namespace Drupal\vib_service\Logger;

use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\vib_service\DatadogClient;
use Psr\Log\LoggerInterface;

/**
 * Class DatadogLog.
 *
 * @package Drupal\vib_service\Logger
 */
class DatadogLog implements LoggerInterface {

  use RfcLoggerTrait;

  /**
   * The client.
   *
   * @var DatadogClient
   */
  protected $client;

  /**
   * DatadogLog constructor.
   *
   * @param \Drupal\vib_service\DatadogClient $client
   *   The client.
   */
  public function __construct(DatadogClient $client) {
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []): void {
    $payload = [
      'message' => $message,
      'context' => $context,
    ];

    $this->client->addLog($payload, $level);
  }

}
