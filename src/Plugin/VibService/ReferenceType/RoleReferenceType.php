<?php

namespace Drupal\vib_service\Plugin\VibService\ReferenceType;

use Drupal\vib_service\Client\VibServiceClientInterface;
use Drupal\vib_service\Plugin\VibService\ReferenceTypeBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @ReferenceType(
 *   id = "role",
 *   label = @Translation("Role"),
 * )
 */
class RoleReferenceType extends ReferenceTypeBase {

  protected $client;

  /**
   * RoleReferenceType constructor.
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\vib_service\Client\VibServiceClientInterface $client
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, VibServiceClientInterface $client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('vib_service.client')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function doGetApiItems() {
    return $this->client->apiGetRoles();
  }


}
