<?php

namespace Drupal\vib_service\Plugin\VibService;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Url;
use Drupal\vib_service\Client\Model\VibFieldableObjectInterface;
use Drupal\vib_service\Client\VibClientException;

/**
 * Class ReferenceTypeBase
 * @package Drupal\vib_service\Plugin\VibService
 */
abstract class ReferenceTypeBase extends PluginBase implements ReferenceTypeInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->configuration['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->configuration['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getApiItems() {
    $items = [];
    try {
      $items = $this->doGetApiItems();
      foreach ($items as $item) {
        if (!$item instanceof VibFieldableObjectInterface) {
          throw new PluginException('Item must implement VibFieldableObjectInterface');
        }
      }
    } catch (VibClientException $e) {
      $this->messenger()
        ->addError($this->t('Could not connect to the VIB Service API. Please provide the correct <a href="@href">settings</a>.', [
          '@href' => Url::fromRoute('vib_service.settings')
            ->toString()
        ]));
    }

    return $items;
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibFieldableObjectInterface[]
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected abstract function doGetApiItems();

}
