<?php

namespace Drupal\vib_service\Plugin\VibService;

/**
 * Interface ReferenceTypeInterface
 * @package Drupal\vib_service\Plugin\VibService
 */
interface ReferenceTypeInterface {

  /**
   * @return string
   */
  public function id();

  /**
   * @return string
   */
  public function label();

  /**
   * @return \Drupal\vib_service\Client\Model\VibFieldableObjectInterface[]
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getApiItems();

}
