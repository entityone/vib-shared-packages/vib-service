<?php

namespace Drupal\vib_service\Plugin\VibService;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Class ReferenceTypePluginManager
 * @package Drupal\vib_service\Plugin\VibService
 */
class ReferenceTypePluginManager extends DefaultPluginManager {

  /**
   * VibApiReferenceTypePluginManager constructor.
   * @param \Traversable $namespaces
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/VibService/ReferenceType', $namespaces, $module_handler,
      'Drupal\vib_service\Plugin\VibService\ReferenceTypeInterface', 'Drupal\vib_service\Annotation\ReferenceType');
  }

  /**
   * @return array
   */
  public function getOptionList() {
    $options = [];
    foreach ($this->getDefinitions() as $definition) {
      $options[$definition['id']] = $definition['label'];
    }

    return $options;
  }
}
