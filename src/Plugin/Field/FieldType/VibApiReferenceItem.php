<?php

namespace Drupal\vib_service\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedDataInterface;

/**
 *
 * @FieldType(
 *   id = "vib_api_reference_item",
 *   label = @Translation("VIB service API reference"),
 *   description = @Translation("This field stores a VIB API item and it's corresponding object."),
 *   category = @Translation("Reference"),
 *   default_widget = "options_select_vib_api_reference",
 *   default_formatter = "list_vib_api_reference",
 *   list_class = "\Drupal\vib_service\Plugin\Field\VibApiReferenceItemList",
 * )
 */
class VibApiReferenceItem extends FieldItemBase {

  /** @var \Drupal\vib_service\Plugin\VibService\ReferenceTypePluginManager $referenceTypeManager */
  protected $referenceTypeManager;
  protected $vibItems = [];

  /**
   * VibApiReferenceItem constructor.
   * @param \Drupal\Core\TypedData\DataDefinitionInterface $definition
   * @param null $name
   * @param \Drupal\Core\TypedData\TypedDataInterface|NULL $parent
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(DataDefinitionInterface $definition, $name = NULL, TypedDataInterface $parent = NULL) {
    parent::__construct($definition, $name, $parent);
    $this->referenceTypeManager = \Drupal::service('plugin.manager.vib_service_reference_type');
    if (!$this->getSetting('target_type')) {
      // When configuring the field, this value is still empty.
      // Shortcut.
      return;
    }    
    $reference_type = $this->referenceTypeManager->createInstance($this->getSetting('target_type'));

    /** @var \Drupal\vib_service\Client\Model\VibFieldableObjectInterface $item */
    foreach ($reference_type->getApiItems() as $item) {
      // Key the array by field key.
      $this->vibItems[$item->getFieldKey()] = $item;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
        'target_type' => '',
      ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element['target_type'] = [
      '#type' => 'select',
      '#title' => t('Type of item to reference'),
      '#options' => $this->referenceTypeManager->getOptionList(),
      '#default_value' => $this->getSetting('target_type'),
      '#required' => TRUE,
      '#disabled' => $has_data,
      '#size' => 1,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Text value'))
      ->addConstraint('Length', ['max' => 255])
      ->setRequired(TRUE);

    $properties['object'] = DataDefinition::create('any')
      ->setLabel(t('Object'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'object' => [
          'type' => 'blob',
          'size' => 'normal',
          'serialize' => TRUE,
        ],
      ],
      'indexes' => [
        'value' => ['value'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();

    if ($vib_code = $this->getCode()) {
      // Save VIB object in db so we can use it later on without consulting API over and over again.
      $this->set('object', $this->vibItems[$vib_code]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->getCode()) && !empty($this->getVibObject());
  }

  /**
   * @return string
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getCode() {
    return $this->get('value')->getValue();
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibFieldableObjectInterface
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getVibObject() {
    return $this->get('object')->getValue();
  }
  
}
