<?php

namespace Drupal\vib_service\Plugin\Field\FieldWidget;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\vib_service\Plugin\VibService\ReferenceTypePluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @FieldWidget(
 *   id = "options_select_vib_api_reference",
 *   label = @Translation("Select list"),
 *   field_types = {
 *     "vib_api_reference_item"
 *   },
 *   multiple_values = TRUE
 * )
 */
class OptionsSelectVibApiReferenceWidget extends OptionsSelectWidget implements ContainerFactoryPluginInterface {

  protected $pluginManager;
  protected $vibItems = [];

  /**
   * OptionsSelectVibApiReferenceWidget constructor.
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   * @param array $settings
   * @param array $third_party_settings
   * @param \Drupal\vib_service\Plugin\VibService\ReferenceTypePluginManager $plugin_manager
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ReferenceTypePluginManager $plugin_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    /** @var \Drupal\vib_service\Plugin\VibService\ReferenceTypeInterface $reference_type */
    if (!empty($this->getFieldSetting('target_type')) && (!$reference_type = $plugin_manager->createInstance($this->getFieldSetting('target_type')))) {
      throw new PluginNotFoundException($this->getFieldSetting('target_type'));
    }

    if (!empty($reference_type)) {
      /** @var \Drupal\vib_service\Client\Model\VibFieldableObjectInterface $item */
      foreach ($reference_type->getApiItems() as $item) {
        // Key the array by field key.
        $this->vibItems[$item->getFieldKey()] = $item;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('plugin.manager.vib_service_reference_type')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function supportsGroups() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getOptions(FieldableEntityInterface $entity) {
    if (!isset($this->options)) {
      $this->options = [];

      // Add an empty option if the widget needs one.
      if ($empty_label = $this->getEmptyLabel()) {
        $this->options = ['_none' => $empty_label] + $this->options;
      }

      /** @var \Drupal\vib_service\Client\Model\VibFieldableObjectInterface $item */
      foreach ($this->vibItems as $item) {
        $this->options[$item->getFieldKey()] = $item->getFieldLabel();
      }
    }

    return $this->options;
  }

}
