<?php

namespace Drupal\vib_service\Plugin\Field;

use Drupal\Core\Field\FieldItemList;

/**
 * Class VibApiReferenceItemList
 * @package Drupal\vib_service
 */
class VibApiReferenceItemList extends FieldItemList {

  /**
   * {@inheritdoc}
   */
  public function referencedVibObjects() {
    if (empty($this->list)) {
      return array();
    }

    $objects = [];
    foreach ($this->list as $delta => $item) {
      $objects[$delta] = $item->getVibObject();
    }

    // Ensure the returned array is ordered by deltas.
    ksort($objects);

    return $objects;
  }
}
