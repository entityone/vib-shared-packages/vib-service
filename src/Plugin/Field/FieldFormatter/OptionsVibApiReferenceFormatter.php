<?php

namespace Drupal\vib_service\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\options\Plugin\Field\FieldFormatter\OptionsDefaultFormatter;

/**
 * @FieldFormatter(
 *   id = "list_vib_api_reference",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "vib_api_reference_item",
 *   }
 * )
 */
class OptionsVibApiReferenceFormatter extends OptionsDefaultFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // Only collect allowed options if there are actually items to display.
    if ($items->count()) {
      /** @var \Drupal\vib_service\Plugin\Field\FieldType\VibApiReferenceItem $item */
      foreach ($items as $delta => $item) {
        if(!$object = $item->getVibObject()){
            continue;
        }

        $elements[$delta] = $object->render();
      }
    }

    return $elements;
  }
}
