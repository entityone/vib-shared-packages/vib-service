<?php

namespace Drupal\vib_service\Plugin\migrate\source;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\vib_service\Client\Model\VibObjectInterface;
use Drupal\vib_service\Client\VibServiceClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VibServiceBase
 * @package Drupal\vib_service\Plugin\migrate\source
 */
abstract class VibServiceBase extends SourcePluginBase implements ContainerFactoryPluginInterface {

  protected $client;
  protected $baseUri;
  protected $objects;

  /**
   * VibServiceBase constructor.
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   * @param \Drupal\vib_service\Client\VibServiceClientInterface $client
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @throws \Drupal\migrate\MigrateException
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, VibServiceClientInterface $client, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->client = $client;
    $this->baseUri = $config_factory->get('vib_service.settings')->get('api_endpoint');
    $this->objects = $this->getApiObjects();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('vib_service.client'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return 'https://services-staging.vib.be/api/v1' . $this->getApiPath();
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $objects = array_map(function (VibObjectInterface $object) {
      return $object->toJson();
    }, $this->objects);

    return new \ArrayIterator($objects);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    if ($object = reset($this->objects)) {
      $keys = array_keys($object->toJson());
      return array_combine($keys, $keys);
    }

    return [];
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibObjectInterface[]
   * @throws \Drupal\migrate\MigrateException
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function getApiObjects() {
    $objects = $this->doApiCall();
    foreach ($objects as $object) {
      if (!$object instanceof VibObjectInterface) {
        throw new MigrateException('All objects have to be of type "VibObjectInterface"');
      }

      if (!$object->toJson()) {
        throw new MigrateException('Export to JSON cannot be empty');
      }
    }

    return $objects;
  }


  /**
   * Returns the API path this migration is for.
   * For example: /users/list
   *
   * @return string
   */
  protected abstract function getApiPath();

  /**
   * @return \Drupal\vib_service\Client\Model\VibObjectInterface[]
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected abstract function doApiCall();

}
