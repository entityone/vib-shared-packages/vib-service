<?php

namespace Drupal\vib_service\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\vib_service\Client\Model\Request\VibUserListRequest;
use Drupal\vib_service\Client\VibClientException;
use Drupal\vib_service\Client\VibServiceClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VibServiceSettingsForm
 * @package Drupal\vib_service\Form
 */
class VibServiceSettingsForm extends ConfigFormBase {

  protected $vibServiceClient;

  /**
   * VibServiceSettingsForm constructor.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\vib_service\Client\VibServiceClientInterface $client
   */
  public function __construct(ConfigFactoryInterface $config_factory, VibServiceClientInterface $client) {
    parent::__construct($config_factory);
    $this->vibServiceClient = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('vib_service.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['vib_service.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vib_services_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vib_service.settings');

    try {
      $this->vibServiceClient->apiGetRoles(FALSE);
    }
    catch (VibClientException $e) {
      $this->messenger()
        ->addError($this->t('Could not connect to VIB services. Are the credentials you provided valid?'));
    }

    $form['api_endpoint'] = [
      '#title' => $this->t('API endpoint'),
      '#type' => 'textfield',
      '#default_value' => $config->get('api_endpoint'),
      '#description' => $this->t('Endpoint to execute API calls (orders, roles, products, ...)'),
      '#required' => TRUE,
    ];

    $form['token_endpoint'] = [
      '#title' => $this->t('Bearer token endpoint'),
      '#type' => 'textfield',
      '#default_value' => $config->get('token_endpoint'),
      '#description' => $this->t('Endpoint to fetch the Bearer token'),
      '#required' => TRUE,
    ];

    $form['client_id'] = [
      '#title' => $this->t('Client ID'),
      '#type' => 'textfield',
      '#default_value' => $config->get('client_id'),
      '#required' => TRUE,
    ];

    $form['client_secret'] = [
      '#title' => $this->t('Client secret'),
      '#type' => 'textfield',
      '#maxlength' => 1024,
      '#default_value' => $config->get('client_secret'),
      '#required' => TRUE,
    ];

    $form['cache_time'] = [
      '#title' => $this->t('API cache time'),
      '#description' => $this->t('The time teams, roles, .. are cached by the API. <strong>NOTE: lowering the cache time will increase the amount of calls to VIB services!</strong>'),
      '#type' => 'select',
      '#options' => [
        2678400 => $this->t('30 days'),
        604800 => $this->t('7 days'),
        86400 => $this->t('1 day'),
      ],
      '#default_value' => $config->get('cache_time') ?? 604800,
    ];

    $form['datadog_log'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log exceptions / debug info'),
      '#description' => $this->t('Send errors and logs to datadog'),
      '#default_value' => $config->get('datadog_log'),
    ];

    $states = [
      'required' => [
        ':input[name="datadog_log"]' => ['checked' => TRUE],
      ],
    ];
    $form['datadog_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Datadog'),
      'datadog_environment' => [
        '#title' => $this->t('Environment'),
        '#type' => 'select',
        '#options' => [
          'local' => $this->t('Local / develop'),
          'staging' => $this->t('Staging / release'),
          'production' => $this->t('Production / master'),
        ],
        '#default_value' => $config->get('datadog_environment'),
        '#states' => $states,
      ],
      'datadog_source' => [
        '#title' => $this->t('Source'),
        '#type' => 'textfield',
        '#default_value' => $config->get('datadog_source'),
        "#field_prefix" => 'vib-',
        '#description' => $this->t('The integration name associated with your log. Eg: conferences. "vib-" will be prefixed automatically'),
        '#states' => $states,
      ],
      '#states' => [
        'visible' => [
          ':input[name="datadog_log"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$this->currentUser()->hasPermission('administer vib service')) {
      $form_state->setError($form, 'Only administrators can save these settings.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('vib_service.settings');

    $config->set('api_endpoint', $form_state->getValue('api_endpoint'))
      ->set('token_endpoint', $form_state->getValue('token_endpoint'))
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('cache_time', $form_state->getValue('cache_time'))
      ->set('datadog_log', $form_state->getValue('datadog_log'))
      ->set('datadog_environment', $form_state->getValue('datadog_environment'))
      ->set('datadog_source', str_replace([
        ' ',
        '_',
      ], '-', strtolower($form_state->getValue('datadog_source'))))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
