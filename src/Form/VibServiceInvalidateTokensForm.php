<?php


namespace Drupal\vib_service\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\vib_service\Client\VibServiceClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VibServiceInvalidateTokensForm.
 *
 * @package Drupal\vib_service\Form
 */
class VibServiceInvalidateTokensForm extends FormBase {

  /**
   * The state system.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * VibServiceInvalidateTokensForm constructor.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state system.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vib_service_invalidate_tokens_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['info'] = [
      '#type' => 'item',
      '#markup' => 'This will invalidate the bearer tokens and fetch them again on the next call.',
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Invalidate tokens'),
        '#button_type' => 'primary',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->state->delete(VibServiceClient::STATE_BEARER_TOKEN);

    $this->messenger()
      ->addMessage($this->t('All bearer tokens have been invalidated and will be renewed'));
  }

}
