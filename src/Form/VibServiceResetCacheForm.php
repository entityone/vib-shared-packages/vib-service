<?php

namespace Drupal\vib_service\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The VIB services reset cache form.
 */
class VibServiceResetCacheForm extends ConfirmFormBase {

  const CACHE_KEYS = [
    'vib_service.client.teams',
    'vib_service.client.roles',
  ];

  /**
   * The cache bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * Constructs the reset cache form class.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache bin.
   */
  public function __construct(CacheBackendInterface $cache, MessengerInterface $messenger) {
    $this->cache = $cache;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache.default'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you wish to reset the API cache?');
  }

  /**
   * {@inheritDoc}
   */
  public function getDescription() {
    return $this->t('The following cache entries will be reset: <pre>@entries</pre>', [
      '@entries' => implode("\r\n", array_map(function ($key) { return ' - ' . $key; }, static::CACHE_KEYS)),
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('vib_service.settings');
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'vib_service_reset_cache_form';
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Delete the cache for these keys to force them to be retrieved from the API.
    foreach (static::CACHE_KEYS as $key) {
      $this->cache->delete($key);
    }

    $this->messenger->addStatus($this->t('The following cache entries have been reset: <pre>@entries</pre>', [
      '@entries' => implode("\r\n", array_map(function ($key) { return ' - ' . $key; }, static::CACHE_KEYS)),
    ]));
  }

}
