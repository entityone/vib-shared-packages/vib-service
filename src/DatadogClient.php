<?php

namespace Drupal\vib_service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Session\AccountInterface;
use GuzzleHttp\RequestOptions;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class DatadogClient.
 *
 * @package Drupal\vib_service
 */
class DatadogClient {

  const URI = 'https://http-intake.logs.datadoghq.eu/v1/input';
  const API_KEY = '18ed04190dd37877973cbf582928109c';

  /**
   * The guzzle client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The current request.
   *
   * @var \GuzzleHttp\Psr7\Request
   */
  protected $request;

  /**
   * The account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * DatadogClient constructor.
   *
   * @param \Drupal\Core\Http\ClientFactory $client_factory
   *   The client factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   */
  public function __construct(ClientFactory $client_factory, ConfigFactoryInterface $config_factory, RequestStack $request_stack, AccountInterface $account) {
    $this->config = $config_factory->get('vib_service.settings');
    $this->request = $request_stack->getCurrentRequest();
    $this->account = $account;

    $this->client = $client_factory->fromOptions([
      'base_uri' => self::URI,
      RequestOptions::HEADERS => [
        'Content-Type' => 'application/json',
        'DD-API-KEY' => self::API_KEY,
      ],
    ]);

  }

  /**
   * Creates a HTTP request.
   *
   * @param string $method
   *   The HTTP method.
   * @param string $path
   *   The relative path.
   * @param array $options
   *   The request options.
   *
   * @return array
   *   The client response.
   */
  protected function request($method, $path, array $options = []) {
    try {
      /** @var \GuzzleHttp\Psr7\Response $response */
      $response = $this->client->request($method, $path, $options);
    } catch (\Exception $e) {
      return [];
    }

    if ($response->getStatusCode() == 200 || $response->getStatusCode() == 201) {
      return Json::decode($response->getBody()->getContents());
    }

    return [];
  }

  /**
   * Sends log tot Datadog.
   *
   * @param array $payload
   *   The payload.
   * @param integer $level
   *   The severity.
   *
   * @return array|void
   */
  public function addLog(array $payload, int $level) {
    if (!$this->config->get('datadog_log')) {
      // Datadog not enabled.
      return;
    }
    if (!$environment = $this->config->get('datadog_environment')) {
      return;
    }
    if (!$source = $this->config->get('datadog_source')) {
      return;
    }

    $payload['ddsource'] = 'vib-' . $source;
    $payload['ddtags'] = 'ENV:' . $environment;
    $payload['hostname'] = $this->request->getSchemeAndHttpHost();
    $payload['status'] = $level;

    $levels = RfcLogLevel::getLevels();
    $payload['level'] = $levels[$level] ?? NULL;
    // Set context.
    $payload['context'] += [
      'channel' => 'Unknown',
      'uid' => $this->account->id() ?: 0,
      'request_uri' => $this->request->getUri(),
      'referer' => $this->request->headers->get('Referer', ''),
      'ip' => $this->request->getClientIP(),
      'timestamp' => time(),
    ];

    return $this->request('POST', '', [RequestOptions::BODY => Json::encode($payload)]);
  }


}
