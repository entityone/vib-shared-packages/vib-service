<?php

namespace Drupal\vib_service\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a ReferenceType annotation object.
 *
 * @Annotation
 */
class ReferenceType extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

}
