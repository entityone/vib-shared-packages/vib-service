This module integrates Drupal with the VIB Service

# Installation

* Add the following GIT repository to your composer.json repositories:

```json
{
    "type": "git",
    "url": "https://gitlab.com/entityone/vib-shared-packages/vib-service.git"
}
```

* Download module and dependent packages by running `composer require drupal/vib_service:version`
  For example `composer require drupal/vib_service:^1.0-alpha1`

  Available versions: https://gitlab.com/entityone/vib-shared-packages/vib-service/-/tags
* Install module by running `drush en vib_service`

## Updating from 1.x

__IMPORTANT__: Do not update from 1.x if you do not know what you are doing.
You will have to edit namespaces and custom code which relies on this module.
__IMPORTANT 2__: Make sure you run the update hooks. If no update hooks were found apply <https://www.drupal.org/project/drupal/issues/2738879>

# Configuration

* Navigate to `/admin/config/services/vib-service/settings`, and fill out the form.
* For the endpoint use `https://services.vib.be/api/v1` (or corresponding staging environment)
