This module integrates Drupal with the VIB forms service

# Installation

* Install module by running `drush en vib_forms`

# Configuration

* Navigate to `/admin/config/services/vib-service/forms`, and fill out the form
* Add a field of type `VIB Form` to the content entity type you want to render forms on
* Fill out the field and the form will be embedded accordingly
