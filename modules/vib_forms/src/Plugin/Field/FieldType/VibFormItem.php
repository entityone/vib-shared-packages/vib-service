<?php

namespace Drupal\vib_forms\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'vib_form' field type.
 *
 * @FieldType(
 *   id = "vib_form",
 *   label = @Translation("VIB Form"),
 *   description = @Translation("Stores a reference to a VIB form."),
 *   category = @Translation("Reference"),
 *   default_widget = "vib_form_default",
 *   default_formatter = "vib_form_default",
 * )
 */
class VibFormItem extends FieldItemBase {

  const PRIVACY_MODE_ANONYMOUS = 0;
  const PRIVACY_MODE_PUBLIC = 1;
  const PRIVACY_MODE_VIB_ONLY = 2;
  const PRIVACY_MODE_INVITE_ONLY = 3;

  const PRIVACY_MODE_ALL = [
    self::PRIVACY_MODE_ANONYMOUS => 'Anonymous (Anyone with a link can access, no need to login)',
    self::PRIVACY_MODE_PUBLIC => 'Public (Anyone with a link can access)',
    self::PRIVACY_MODE_VIB_ONLY => 'VIB only (Only VIB users have access)',
    self::PRIVACY_MODE_INVITE_ONLY => 'Invite only (Invite required for access)',
  ];

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['form_guid'] = DataDefinition::create('string')
      ->setLabel(t('Form GUID'))
      ->setRequired(FALSE);

    $properties['form_id'] = DataDefinition::create('string')
      ->setLabel(t('Form ID'))
      ->setRequired(FALSE);

    $properties['privacy_mode'] = DataDefinition::create('string')
      ->setLabel(t('Privacy Mode'))
      ->setDescription(t('Choose who can see this information. This should be the same value as configured in VIB forms.'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'form_guid' => [
          'description' => 'The form GUID.',
          'type' => 'varchar',
          'length' => 255,
        ],
        'form_id' => [
          'description' => 'The form ID.',
          'type' => 'varchar',
          'length' => 255,
        ],
        'privacy_mode' => [
          'description' => 'The privacy mode.',
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();

    return [
      'form_guid' => $random->string(22),
      'form_id' => $random->string(5),
      'privacy_mode' => self::PRIVACY_MODE_ALL[rand(0, 3)],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return $this->form_guid === NULL || $this->form_guid === '' || $this->privacy_mode === NULL || $this->privacy_mode === '';
  }

}
