<?php

namespace Drupal\vib_forms\Plugin\Field\FieldFormatter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\vib_forms\Plugin\Field\FieldType\VibFormItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'vib_form_default' formatter.
 *
 * @FieldFormatter(
 *   id = "vib_form_default",
 *   label = @Translation("Default (embed)"),
 *   field_types = {
 *     "vib_form"
 *   }
 * )
 */
class VibFormDefaultFormatter extends FormatterBase {

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a VibFormDefaultFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(string $plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ConfigFactoryInterface $config_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->config = $config_factory->get('vib_forms.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $actions = [];
      if (\Drupal::currentUser()
          ->hasPermission('manage vib forms') && !empty($items[$delta]->form_id)) {
        $actions = [
          '#theme' => 'vib_forms_tasks',
          '#form_id'=> $items[$delta]->form_id,
          '#weight' => -100,
          '#attached' => [
            'library' => ['vib_forms/vib-forms-widget']
          ],
        ];
      }

      $elements[$delta] = [
        'actions' => $actions,
        'form' => [
          '#type' => 'inline_template',
          '#template' => '<link href="{{ endpoint }}/vib-form-editor-embed.css" rel="stylesheet"/>
<div id="vib-forms-app" class="vib-embed-form" guid="{{ guid }}" anonymous-mode="{{ anonymous_mode }}"></div>
<script type="text/javascript" src="{{ endpoint }}/vib-form-editor-embed.min.js"></script>',
          '#context' => [
            'endpoint' => $this->config->get('endpoint'),
            'guid' => $items[$delta]->form_guid,
            'anonymous_mode' => ((int) $items[$delta]->privacy_mode === VibFormItem::PRIVACY_MODE_ANONYMOUS) ? 'true' : 'false',
          ],
        ],
      ];
    }

    return $elements;
  }

}
