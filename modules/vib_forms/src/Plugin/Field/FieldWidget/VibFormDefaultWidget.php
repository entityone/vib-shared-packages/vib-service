<?php

namespace Drupal\vib_forms\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mt_price\Price;
use Drupal\vib_forms\Plugin\Field\FieldType\VibFormItem;

/**
 * Plugin implementation of the 'vib_form_default' widget.
 *
 * @FieldWidget(
 *   id = "vib_form_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "vib_form"
 *   }
 * )
 */
class VibFormDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['#type'] = 'fieldset';
    $element['form_guid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form GUID'),
      '#default_value' => $items[$delta]->form_guid ?? NULL,
      '#description' => $this->t('Form GUID as defined in the VIB form tool, eg "20113261-aacb-4a37-8a53-8d70aa79f748"'),
    ];

    $element['form_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form ID'),
      '#default_value' => $items[$delta]->form_id ?? NULL,
      '#description' => $this->t('Form ID as defined in the VIB form tool, eg "1179"'),
    ];

    $element['privacy_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Privacy mode'),
      '#options' => VibFormItem::PRIVACY_MODE_ALL,
      '#empty_option' => $this->t('- Choose -'),
      '#default_value' => $items[$delta]->privacy_mode ?? NULL,
      '#description' => $this->t('Choose who can see this information. This should be the same value as configured in VIB forms'),
    ];

    return $element;
  }

}
