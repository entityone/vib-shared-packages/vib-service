<?php

namespace Drupal\vib_service_notifications\Plugin\Mail;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Mail\MailInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\vib_service\Client\VibClientException;
use Drupal\vib_service_notifications\Client\Model\Request\VibSendEmailRequest;
use Drupal\vib_service_notifications\Client\VibServiceNotificationClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Modify the Drupal mail system to use VIB notifications when sending emails.
 *
 * @Mail(
 *   id = "vib_notification_mail",
 *   label = @Translation("VIB notification mailer"),
 *   description = @Translation("Sends the message using VIB notifications.")
 * )
 */
class VibNotificationMail implements MailInterface, ContainerFactoryPluginInterface {

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $notificationConfig;

  /**
   * The VIB client.
   *
   * @var \Drupal\vib_service_notifications\Client\VibServiceNotificationClientInterface
   */
  protected $client;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * VibNotificationMail constructor.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $settings
   *   The settings.
   * @param \Drupal\vib_service_notifications\Client\VibServiceNotificationClientInterface $client
   *   The VIB client.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(ImmutableConfig $settings, VibServiceNotificationClientInterface $client, RendererInterface $renderer) {
    $this->notificationConfig = $settings;
    $this->client = $client;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('config.factory')
        ->get('vib_service_notifications.settings'),
      $container->get('vib_service_notifications.client'),
      $container->get('renderer')
    );
  }

  /**
   * Wrap the e-mail body HTML e-mails.
   *
   * @param array $message
   *   A message array, as described in hook_mail_alter().
   *
   * @return array
   *   The formatted $message.
   */
  public function format(array $message) {
    $render = [
      '#theme' => 'vib_notification_email',
      '#message' => $message,
    ];
    $message['body'] = $this->renderer->renderRoot($render);

    return $message;
  }

  /**
   * Send the e-mail message.
   *
   * @param array $message
   *   A message array, as described in hook_mail_alter().
   *   $message['params'] may contain additional parameters.
   *
   * @return bool
   *   TRUE if the mail was successfully accepted, FALSE otherwise.
   *
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function mail(array $message) {
    if (isset($message['vib_email']) && $message['vib_email'] instanceof VibSendEmailRequest) {
      // VIB email object was provided in params. Use that over other values.
      return $this->client->sendEmail($message['vib_email']);
    }

    $email = VibSendEmailRequest::createFromMessage($message, $this->notificationConfig);
    try {
      return $this->client->sendEmail($email);
    } catch (VibClientException $e) {

    }
    return FALSE;
  }

}
