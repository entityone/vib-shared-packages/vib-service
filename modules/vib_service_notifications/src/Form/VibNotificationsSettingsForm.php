<?php

namespace Drupal\vib_service_notifications\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\vib_service\Client\VibClientException;
use Drupal\vib_service\Client\VibServiceClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VibNotificationsSettingsForm.
 *
 * @package Drupal\vib_service_notifications\Form
 */
class VibNotificationsSettingsForm extends ConfigFormBase {

  /**
   * The VIB client.
   *
   * @var \Drupal\vib_service\Client\VibServiceClientInterface
   */
  protected $vibServiceClient;

  /**
   * VibServiceSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\vib_service\Client\VibServiceClientInterface $client
   *   The VIB client.
   */
  public function __construct(ConfigFactoryInterface $config_factory, VibServiceClientInterface $client) {
    parent::__construct($config_factory);
    $this->vibServiceClient = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('vib_service.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vib_notifications_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['vib_service_notifications.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('vib_service_notifications.settings');
    try {
      $this->vibServiceClient->apiGetRoles();
    }
    catch (VibClientException $e) {
      $this->messenger()
        ->addError($this->t('Could not connect to VIB services. Are the credentials <a href=":url">you provided</a> valid?', [
          ':url' => Url::fromRoute('vib_service.settings')->toString(),
        ]));

      return $form;
    }

    $form['api_endpoint'] = [
      '#title' => $this->t('API endpoint'),
      '#type' => 'textfield',
      '#default_value' => $settings->get('api_endpoint'),
      '#description' => $this->t('Endpoint to execute API calls to (mails, notifications, templates, ...)'),
      '#required' => TRUE,
    ];

    $form['notify_app_id'] = [
      '#title' => $this->t('Notify app ID'),
      '#type' => 'textfield',
      '#default_value' => $settings->get('notify_app_id'),
      '#description' => $this->t('The notify app ID as configured on https://services.vib.be/admin/#/app/{APP_ID}/notification'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('vib_service_notifications.settings');

    $settings
      ->set('api_endpoint', $form_state->getValue('api_endpoint'))
      ->set('notify_app_id', $form_state->getValue('notify_app_id'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
