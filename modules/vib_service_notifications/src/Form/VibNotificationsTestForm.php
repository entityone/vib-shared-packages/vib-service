<?php

namespace Drupal\vib_service_notifications\Form;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VibNotificationsTestForm.
 *
 * @package Drupal\vib_service_notifications\Form
 */
class VibNotificationsTestForm extends FormBase {

  /**
   * The current account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The mail Manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * VibNotificationsTestForm constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current account.
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *   The mail manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   */
  public function __construct(AccountInterface $account, MailManagerInterface $mailManager, FileSystemInterface $fileSystem) {
    $this->account = $account;
    $this->mailManager = $mailManager;
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('plugin.manager.mail'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vib_notifications_test_email_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Display a warning if Mailgun is not a default mailer.
    $sender = \Drupal::config('mailsystem.settings')->get('defaults.sender');
    if ($sender != 'vib_notification_mail') {
      $this->messenger()
        ->addMessage($this->t('VIB notification is not a default Mailsystem plugin. You may update settings at @link.', [
          '@link' => Link::createFromRoute($this->t('here'), 'mailsystem.settings')
            ->toString(),
        ]), 'warning');
    }

    $form['to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('To'),
      '#required' => TRUE,
      '#description' => $this->t('Email will be sent to this address. You can use commas to separate multiple recipients.'),
      '#default_value' => $this->account->getEmail(),
    ];

    $form['body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#required' => TRUE,
      '#default_value' => $this->t('Howdy!

If this e-mail is displayed correctly and delivered sound and safe, congrats! You have successfully configured VIB notifications.'),
    ];

    $form['include_attachment'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include attachment'),
      '#description' => $this->t('If checked, an image will be included as an attachment with the test e-mail.'),
    ];

    $form['extra'] = [
      '#type' => 'details',
      '#title' => $this->t('Additional parameters'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => $this->t('You may test more parameters to make sure they are working.'),
    ];

    $form['extra']['reply_to'] = [
      '#type' => 'email',
      '#title' => $this->t('Reply-To'),
    ];

    $form['extra']['cc'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CC'),
      '#description' => $this->t('You can use commas to separate multiple recipients.'),
    ];

    $form['extra']['bcc'] = [
      '#type' => 'textfield',
      '#title' => $this->t('BCC'),
      '#description' => $this->t('You can use commas to separate multiple recipients.'),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
      '#button_type' => 'primary',
    ];

    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('vib_service_notifications.settings'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $to = $form_state->getValue('to');

    $params = [
      'subject' => $this->t('VIB notifications works!'),
      'body' => [$form_state->getValue('body')],
    ];

    if (!empty($form_state->getValue('include_attachment'))) {
      $params['attachments'][] = $this->fileSystem->realpath('core/misc/druplicon.png');
    }

    // Add CC / BCC values if they are set.
    if ($cc = $form_state->getValue('cc')) {
      $params['cc'] = explode(',', $cc);
    }
    if ($bcc = $form_state->getValue('bcc')) {
      $params['bcc'] = explode(',', $bcc);
    }

    $result = $this->mailManager->mail('vib_service_notifications', 'test_email', $to, $this->account->getPreferredLangcode(), $params, $form_state->getValue('reply_to'), TRUE);

    if ($result['result'] === TRUE) {
      $this->messenger()
        ->addMessage($this->t('Successfully sent message to %to.', ['%to' => $to]));
    }
    else {
      $this->messenger()
        ->addMessage($this->t('Something went wrong. Please check logs for details.'), 'warning');
    }
  }

}
