<?php

namespace Drupal\vib_service_notifications\Client\Model\Request;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\vib_service\Client\Model\VibMetaData;
use Drupal\vib_service\Client\Model\VibObjectInterface;
use Drupal\vib_service\Client\VibClientException;
use Drupal\vib_service_notifications\Client\Model\VibEmailContact;

/**
 * Class VibSendEmailRequest.
 *
 * @package Drupal\vib_service_notifications\Client\Model\Request
 */
class VibSendEmailRequest implements VibObjectInterface {

  /**
   * The app id.
   *
   * @var string
   */
  protected $appId;

  /**
   * The external id.
   *
   * @var string
   */
  protected $externalId;

  /**
   * The subject.
   *
   * @var string
   */
  protected $subject;

  /**
   * The body.
   *
   * @var string
   */
  protected $body;

  /**
   * The notification type id.
   *
   * @var string
   */
  protected $notificationTypeId;

  /**
   * The email to list.
   *
   * @var \Drupal\vib_service_notifications\Client\Model\VibEmailContact[]
   */
  protected $emailTo;

  /**
   * The email cc list.
   *
   * @var \Drupal\vib_service_notifications\Client\Model\VibEmailContact[]
   */
  protected $emailCc;

  /**
   * The email bcc list.
   *
   * @var \Drupal\vib_service_notifications\Client\Model\VibEmailContact[]
   */
  protected $emailBcc;

  /**
   * The metadata.
   *
   * @var \Drupal\vib_service\Client\Model\VibMetaData|null
   */
  protected $metadata;

  /**
   * The scheduled at date.
   *
   * @var \Drupal\Core\Datetime\DrupalDateTime|null
   */
  protected $scheduledAt;

  /**
   * VibSendEmailRequest constructor.
   *
   * @param string $app_id
   *   The app id.
   * @param string $external_id
   *   The external id.
   * @param string $subject
   *   The subject.
   * @param string $body
   *   The body.
   * @param string $notification_type_id
   *   The email from.
   * @param \Drupal\vib_service_notifications\Client\Model\VibEmailContact[] $email_to
   *   The mail to list.
   * @param \Drupal\vib_service_notifications\Client\Model\VibEmailContact[] $email_cc
   *   The mail cc list.
   * @param \Drupal\vib_service_notifications\Client\Model\VibEmailContact[] $email_bcc
   *   The mail bcc list.
   * @param \Drupal\vib_service\Client\Model\VibMetaData|null $metadata
   *   The metadata.
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $scheduled_at
   *   The scheduled at date.
   *
   * @throws \Drupal\vib_service\Client\VibClientException
   */
  public function __construct(string $app_id, string $external_id, string $subject, string $body, string $notification_type_id, array $email_to, array $email_cc = [], array $email_bcc = [], VibMetaData $metadata = NULL, DrupalDateTime $scheduled_at = NULL) {
    $required_props = ['app_id', 'email_to'];
    foreach ($required_props as $prop) {
      if (!empty(${$prop})) {
        continue;
      }
      throw new VibClientException($prop . ' cannot be empty.');
    }

    if ((empty($subject) && empty($body) && empty($notification_type_id)) ||
      (!empty($subject) && !empty($body) && !empty($notification_type_id))) {
      throw new VibClientException('Provide body and subject or a notification type');
    }

    $this->appId = $app_id;
    $this->externalId = $external_id;
    $this->subject = $subject;
    $this->body = $body;
    $this->notificationTypeId = $notification_type_id;
    $this->emailTo = $email_to;
    $this->emailCc = $email_cc;
    $this->emailBcc = $email_bcc;
    $this->metadata = $metadata;
    $this->scheduledAt = $scheduled_at;
  }

  /**
   * Returns the app id.
   *
   * @return string
   *   The app id.
   */
  public function getAppId() {
    return $this->appId;
  }

  /**
   * Returns the external id.
   *
   * @return string
   *   The external id.
   */
  public function getExternalId() {
    return $this->externalId;
  }

  /**
   * Returns the subject.
   *
   * @return string
   *   The subject.
   */
  public function getSubject() {
    return $this->subject;
  }

  /**
   * Returns the body.
   *
   * @return string
   *   The body.
   */
  public function getBody() {
    return $this->body;
  }

  /**
   * Returns the notification type id.
   *
   * @return string
   *   The notification type id.
   */
  public function getNotificationTypeId() {
    return $this->notificationTypeId;
  }

  /**
   * Returns the email to list.
   *
   * @return \Drupal\vib_service_notifications\Client\Model\VibEmailContact[]
   *   The email to list.
   */
  public function getEmailTo(): array {
    return $this->emailTo;
  }

  /**
   * Returns the email cc list.
   *
   * @return \Drupal\vib_service_notifications\Client\Model\VibEmailContact[]
   *   The email cc list.
   */
  public function getEmailCc(): array {
    return $this->emailCc;
  }

  /**
   * Returns the email bcc list.
   *
   * @return \Drupal\vib_service_notifications\Client\Model\VibEmailContact[]
   *   The email bcc list.
   */
  public function getEmailBcc(): array {
    return $this->emailBcc;
  }

  /**
   * Returns the metadata.
   *
   * @return \Drupal\vib_service\Client\Model\VibMetaData
   *   The metadata.
   */
  public function getMetadata(): ?VibMetaData {
    return $this->metadata;
  }

  /**
   * Returns the scheduled date.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|null
   *   The scheduled date.
   */
  public function getScheduledAt(): ?DrupalDateTime {
    return $this->scheduledAt;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'AppId' => $this->getAppId(),
      'ExternalId' => $this->getExternalId(),
      'UserId' => NULL,
      'Subject' => $this->getSubject(),
      'Body' => $this->getBody(),
      'NotificationTypeId' => $this->getNotificationTypeId(),
      'EmailTo' => array_map(function (VibEmailContact $contact) {
        return $contact->toJson();
      }, $this->getEmailTo()),
      'EmailCC' => array_map(function (VibEmailContact $contact) {
        return $contact->toJson();
      }, $this->getEmailCc()),
      'EmailBCC' => array_map(function (VibEmailContact $contact) {
        return $contact->toJson();
      }, $this->getEmailBcc()),
      'BodyHtml' => TRUE,
      'Attachments' => [],
      'Metadata' => $this->getMetadata() ? $this->getMetadata()
        ->toJson() : NULL,
      'scheduledAt' => $this->getScheduledAt() ? $this->scheduledAt->format('Y-m-d\TH:i:s.v\Z') : NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return NULL;
  }

  /**
   * Creates a request object by a given Drupal mail message.
   *
   * @param array $message
   *   The message.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The config.
   *
   * @return \Drupal\vib_service_notifications\Client\Model\Request\VibSendEmailRequest
   *   The request object.
   *
   * @throws \Drupal\vib_service\Client\VibClientException
   */
  public static function createFromMessage(array $message, ImmutableConfig $config) {
    $to_name = $message['params']['to']['name'] ?? $message['to'];

    $to = new VibEmailContact(NULL, $message['to'], $to_name);

    return new static(
      $config->get('notify_app_id'),
      '',
      $message['subject'],
      $message['body'],
      '',
      [$to]
    );
  }

}
