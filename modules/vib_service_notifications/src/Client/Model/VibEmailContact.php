<?php

namespace Drupal\vib_service_notifications\Client\Model;

use Drupal\vib_service\Client\Model\VibObjectInterface;
use Drupal\vib_service\Client\VibClientException;

/**
 * Class VibEmailContact.
 *
 * @package Drupal\vib_service_notifications\Client\Model
 */
class VibEmailContact implements VibObjectInterface {

  /**
   * The VIB user id.
   *
   * @var string
   */
  protected $vibUserId;

  /**
   * The name.
   *
   * @var string
   */
  protected $name;

  /**
   * The email.
   *
   * @var string
   */
  protected $email;

  /**
   * VibEmailContact constructor.
   *
   * @param string|null $vib_user_id
   *   The VIB user ID.
   * @param string|null $name
   *   The name.
   * @param string|null $email
   *   The email.
   *
   * @throws \Drupal\vib_service\Client\VibClientException
   */
  public function __construct(string $vib_user_id = NULL, string $name = NULL, string $email = NULL) {
    if (!empty($vib_user_id) && !empty($email)) {
      throw new VibClientException('You cannot provide bot user id and email.');
    }
    $this->vibUserId = $vib_user_id;
    $this->name = $name;
    $this->email = $email;
  }

  /**
   * Returns the VIB user id.
   *
   * @return string
   *   The VIB user id.
   */
  public function getVibUserId() {
    return $this->vibUserId;
  }

  /**
   * Returns the name.
   *
   * @return string
   *   The name
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Returns the email.
   *
   * @return string
   *   The email.
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * {@inheritdoc}
   */
  public function toJson() {
    return [
      'UserId' => $this->getVibUserId(),
      'Name' => $this->getName(),
      'Email' => $this->getEmail(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromJson(array $json) {
    return NULL;
  }

}
