<?php

namespace Drupal\vib_service_notifications\Client;

use Drupal\vib_service_notifications\Client\Model\Request\VibSendEmailRequest;

/**
 * Interface VibServiceNotificationClientInterface.
 *
 * @package Drupal\vib_service_notifications\Client
 */
interface VibServiceNotificationClientInterface {

  /**
   * Sends mail to given user(s).
   *
   * @param \Drupal\vib_service_notifications\Client\Model\Request\VibSendEmailRequest $request
   *   The request object.
   *
   * @return bool
   *   Flag indicating if the email was sent successfully.
   *
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function sendEmail(VibSendEmailRequest $request);

}
