<?php

namespace Drupal\vib_service_notifications\Client;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\State\StateInterface;
use Drupal\vib_service\Client\VibClientException;
use Drupal\vib_service_notifications\Client\Model\Request\VibSendEmailRequest;
use GuzzleHttp\RequestOptions;

/**
 * Class VibServiceNotificationClient.
 *
 * @package Drupal\vib_service_notifications\Client
 */
class VibServiceNotificationClient implements VibServiceNotificationClientInterface {

  const STATE_BEARER_TOKEN = 'vib_services_notification_bearer_token';

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The client factory.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $clientFactory;

  /**
   * The http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The state storage.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * VibServiceNotificationClient constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   The http client factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state storage.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientFactory $http_client_factory, StateInterface $state) {
    $this->config = $config_factory->get('vib_service_notifications.settings');
    $this->clientFactory = $http_client_factory;
    $this->state = $state;
    $this->httpClient = $this->createFromOptions();
  }

  /**
   * Returns a client.
   *
   * @return \GuzzleHttp\Client
   *   The client.
   */
  protected function createFromOptions() {
    $client = drupal_static(__FUNCTION__);

    if (!isset($client)) {
      $endpoint = $this->config->get('api_endpoint');
      $options = [
        'base_uri' => substr($endpoint, -1, 1) == '/' ? $endpoint : $endpoint . '/',
        'headers' => [
          'Content-Type' => 'application/json',
        ],
      ];

      $client = $this->clientFactory->fromOptions($options);
    }

    return $client;
  }

  /**
   * Refreshes bearer token.
   *
   * @param string $scope
   *   The scope.
   *
   * @return string
   *   The bearer token.
   *
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function refreshBearerToken(string $scope) {
    try {
      $config = \Drupal::config('vib_service.settings');
      $request_options = [
        'form_params' => [
          'client_id' => $config->get('client_id'),
          'client_secret' => $config->get('client_secret'),
          'grant_type' => 'client_credentials',
          'scope' => $scope,
        ],
      ];

      $response = $this->clientFactory->fromOptions()
        ->request('POST', $config->get('token_endpoint'), $request_options);
      $json = Json::decode($response->getBody()->getContents());

      if (empty($json['access_token'])) {
        throw new VibClientException('Could not API fetch bearer token');
      }

      return $json['access_token'];
    }
    catch (\Exception $e) {
      throw new VibClientException($e->getMessage());
    }
  }

  /**
   * Returns (cached) bearer token.
   *
   * @param string $scope
   *   The scope.
   *
   * @return string
   *   The bearer token.
   *
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function getBearerToken($scope = 'orders roles teams users codes') {
    if (!$tokens = $this->state->get(self::STATE_BEARER_TOKEN)) {
      $token = $this->refreshBearerToken($scope);
      // Cache token per scope.
      $tokens[$scope] = $token;
      $this->state->set(self::STATE_BEARER_TOKEN, $tokens);
    }

    if (empty($tokens[$scope])) {
      $token = $this->refreshBearerToken($scope);
      // Cache token per scope.
      $tokens[$scope] = $token;
      $this->state->set(self::STATE_BEARER_TOKEN, $tokens);
    }

    return $tokens[$scope];
  }

  /**
   * Executes request.
   *
   * @param string $method
   *   The HTTP method.
   * @param string $path
   *   The path.
   * @param array $options
   *   The request options.
   * @param bool $return_as_json
   *   Flag indicating if response has to be parsed to JSON.
   *
   * @return array|\GuzzleHttp\Psr7\Response
   *   The response.
   *
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function request(string $method, string $path, array $options = [], $return_as_json = TRUE) {
    $cache_id = $path;
    $cache = &drupal_static(__FUNCTION__);

    if ($method === 'GET' && isset($cache[$cache_id])) {
      return $cache[$path];
    }

    // Keep track of loops to make sure we do not stay here forever.
    // This can occur when VIB services would be "down".
    if (!$loops = &drupal_static(__FUNCTION__ . 'loops')) {
      $loops = 1;
    }
    try {
      // Add Bearer token.
      $options['headers']['Authorization'] = 'Bearer ' . $this->getBearerToken();

      /** @var \GuzzleHttp\Psr7\Response $response */
      $response = $this->httpClient->request($method, $path, $options);
    }
    catch (\Exception $e) {
      if ($e->getCode() === 401 && $loops < 3) {
        // Bearer token is probably expired.
        // Delete expired one and execute request again.
        $this->state->delete(self::STATE_BEARER_TOKEN);
        $loops++;
        return $this->request($method, $path, $options, $return_as_json);
      }

      throw new VibClientException($e->getMessage());
    }

    if ($response->getStatusCode() == 200 || $response->getStatusCode() == 201) {
      if ($return_as_json) {
        // Cache response and return it.
        $cache[$cache_id] = Json::decode($response->getBody()->getContents());
        return $cache[$cache_id];
      }
      return $response;
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function sendEmail(VibSendEmailRequest $request) {
    $options = [
      RequestOptions::JSON => $request->toJson(),
    ];

    $response = $this->request('POST', 'emails', $options, FALSE);
    return $response->getStatusCode() === 200;
  }

  public function sendNotification(){

  }

}
