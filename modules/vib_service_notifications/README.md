This module integrates Drupal with the VIB Service Notifications

# Installation

* Install module by running `drush en vib_service_notifications`

# Configuration

* Make sure you have enabled notifications in your VIB services app
  on `https://services.vib.be/admin/#/app/{APP_ID}/notification`
* Navigate to `/admin/config/services/vib-service/notifications`, and fill out the form.
* Make sure you have configured the mailsystem module accordingly on `/admin/config/system/mailsystem`
