(function($, Drupal, once) {
  Drupal.behaviors.vibWidgetSelect2Init = {
    attach: function (context) {
      $(once('select2-init', 'select.vib-widget-select2', context)).each(function () {
        let that = $(this);
        that.select2({
          allowClear: true,
          placeholder: "- Choose -",
          ajax: {
            // Our middleware endpoint.
            url: window.location.origin + '/api/widget-options/' + that.attr('data-widget-type'),
            type: 'get',
            dataType: 'json',
            delay: 100,
            data: function (params) {
              // This defines the query parameters we'll pass along to our endpoint.
              return {
                keyword: params.term,
                page: params.page || 0,
                pageSize: 100,
              };
            }
          }
        });
      });
    }
  };
})(jQuery, Drupal, once);
