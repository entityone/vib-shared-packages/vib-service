<?php

namespace Drupal\vib_widget;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\State\StateInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class VibWidgetHelper.
 *
 * @package Drupal\vib_widget
 */
class VibWidgetHelper {

  const STATE_VIB_WIDGETS_DEFINITION = 'vib_widgets_definition';

  /**
   * The state system.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The client.
   *
   * @var \Drupal\vib_widget\VibWidgetsClient
   */
  protected $widgetsClient;

  /**
   * Constructs a VibWidgetHelper object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state system.
   * @param \Drupal\vib_widget\VibWidgetsClient $client
   *   The client.
   */
  public function __construct(StateInterface $state, VibWidgetsClient $client) {
    $this->state = $state;
    $this->widgetsClient = $client;
  }

  /**
   * Returns a widget by a given id.
   *
   * @param string $widget_id
   *   The widget id.
   * @return \Drupal\vib_widget\Widget
   *   The widget.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getWidget(string $widget_id) {
    return $this->getWidgets()[$widget_id] ?? NULL;
  }

  /**
   * Returns list of widgets.
   *
   * @return \Drupal\vib_widget\Widget[]
   *   The widgets.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getWidgets() {
    $widgets = [];

    foreach ($this->getWidgetsDefinition() as $definition) {
      $widget = Widget::createFromDefinition($definition);
      $widgets[$widget->getId()] = $widget;
    }

    return $widgets;
  }

  /**
   * Returns the cached widget definition.
   *
   * @param bool $reset
   *   Bool indicating if cache has to be refreshed.
   *
   * @return array
   *   The widget definition.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getWidgetsDefinition(bool $reset = FALSE) {
    if ((!$definition = $this->state->get(self::STATE_VIB_WIDGETS_DEFINITION, [])) || $reset) {
      $this->refreshWidgetsDefinition();
      $definition = $this->state->get(self::STATE_VIB_WIDGETS_DEFINITION);
    }

    return $definition;
  }

  /**
   * Refreshes the widgets definition.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function refreshWidgetsDefinition() {
    $this->state->set(self::STATE_VIB_WIDGETS_DEFINITION, $this->widgetsClient->getWidgetsDefinition());
  }

}
