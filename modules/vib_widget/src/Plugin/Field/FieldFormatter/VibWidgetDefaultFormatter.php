<?php

namespace Drupal\vib_widget\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'vib_widget_default' formatter.
 *
 * @FieldFormatter(
 *   id = "vib_widget_default",
 *   label = @Translation("Default (embed)"),
 *   field_types = {
 *     "vib_widget"
 *   }
 * )
 */
class VibWidgetDefaultFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritDoc}
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, LanguageManagerInterface $language_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    /** @var \Drupal\vib_widget\VibWidgetHelper $widget_helper */
    $widget_helper = \Drupal::service('vib_widget.helper');

    if (empty($langcode)) {
      $langcode = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT);
    }

    foreach ($items as $delta => $item) {
      if (!$item->widget_id) {
        continue;
      }
      if (!$widget = $widget_helper->getWidget($item->widget_id)) {
        continue;
      }

      $properties = $items->widget_properties ?? [];
      if ($widget->getMultilingual()) {
        $properties['lang'] = $langcode;
      }

      $elements[] = [
        '#type' => 'inline_template',
        '#template' => '<div id="{{ widget_id }}" {{ properties | raw }}></div>',
        '#context' => [
          'widget_id' => $widget->getId(),
          'properties' => implode(' ', array_map(function ($attribute, $value) {
            $value = !is_array($value) ? [$value] : $value;
            return $attribute . '="' . implode(',', $value) . '"';
          }, array_keys($properties), $properties)),
        ],
        '#attached' => [
          'library' => ['vib_widget/' . $widget->getLibraryName()]
        ],
      ];
    }

    return $elements;
  }

}
