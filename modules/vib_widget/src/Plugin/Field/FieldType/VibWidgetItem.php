<?php

namespace Drupal\vib_widget\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'vib_widget' field type.
 *
 * @FieldType(
 *   id = "vib_widget",
 *   label = @Translation("VIB Widget"),
 *   description = @Translation("Stores configuration for a VIB widget."),
 *   category = @Translation("Reference"),
 *   default_widget = "vib_widget_default",
 *   default_formatter = "vib_widget_default",
 * )
 */
class VibWidgetItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['widget_id'] = DataDefinition::create('string')
      ->setLabel(t('Widget ID'))
      ->setRequired(FALSE);

    $properties['widget_properties'] = DataDefinition::create('any')
      ->setLabel(t('Widget properties'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'widget_id';
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'widget_id' => [
          'description' => 'The widget ID.',
          'type' => 'varchar',
          'length' => 255,
        ],
        'widget_properties' => [
          'type' => 'blob',
          'size' => 'normal',
          'serialize' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();

    return [
      'widget_id' => $random->string(22),
      'widget_properties' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return $this->widget_id === NULL || $this->widget_id === '';
  }

}
