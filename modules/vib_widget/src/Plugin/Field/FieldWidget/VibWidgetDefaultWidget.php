<?php

namespace Drupal\vib_widget\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\mt_price\Price;
use Drupal\vib_forms\Plugin\Field\FieldType\VibFormItem;
use Drupal\vib_widget\VibWidgetHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Plugin implementation of the 'vib_widget' widget.
 *
 * @FieldWidget(
 *   id = "vib_widget_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "vib_widget"
 *   }
 * )
 */
class VibWidgetDefaultWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The widget helper.
   *
   * @var \Drupal\vib_widget\VibWidgetHelper
   */
  protected $widgetHelper;

  /**
   * The current user object.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, VibWidgetHelper $widget_helper, AccountProxyInterface $current_user) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->widgetHelper = $widget_helper;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('vib_widget.helper'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();
    // Build array of field's parents.
    $parents = array_merge($element['#field_parents'], [
      $field_name,
      $delta,
      'widget_id'
    ]);

    $element['#type'] = 'fieldset';
    $element['#attributes']['data-vib-widget-id'] = $this->generateVibWidgetId($parents);

    $options = [];
    foreach ($this->widgetHelper->getWidgets() as $widget) {
      $options[$widget->getId()] = $widget->getName();
    }

    $element['widget_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Widget'),
      '#title_display' => 'invisible',
      '#options' => $options,
      '#empty_option' => $this->t('- Choose widget -'),
      '#default_value' => $items[$delta]->widget_id ?? NULL,
      '#ajax' => [
        'callback' => [$this, 'ajaxRefreshWidgetProperties'],
        'options' => [
          'query' => [
            'element_parents' => implode('/', $parents),
          ],
        ],
      ],
    ];

    // Use default value stored on item.
    $widget_id = $items[$delta]->widget_id ?? NULL;
    //Check if form_state rebuilding & check if its not a paragraph
    if ($form_state->isRebuilding()  && !array_key_exists('#paragraphs_mode', $form_state->getTriggeringElement())) {
      // Use default value stored in form state.
      // "isRebuilding" indicates that ajax call was executed.
      $widget_id = NestedArray::getValue($form_state->getValues(), $parents);
    }

    $element['widget_properties'] = [
      '#type' => 'container',
    ];

    if ($widget_id && ($widget = $this->widgetHelper->getWidget($widget_id))) {
      $element['widget_id']['#description'] = $widget->getDescription();

      foreach ($widget->getProperties() as $property) {
        $default_value = $items[$delta]->widget_properties[$property->getAttribute()] ?? $property->getDefault();
        $element['widget_properties'][$property->getAttribute()] = $property->buildElement($default_value);
        $element['widget_properties'][$property->getAttribute()]['#default_value'] = $default_value;
      }
    }

    $url = Url::fromRoute('vib_widget.settings');
    if ($url->access($this->currentUser)) {
      $element['info'] = [
        '#markup' => $this->t('Don\'t see your widget in the list? <a href=":url" target="_blank">re-save the config form</a>, and refresh this page.', [
          ':url' => $url->toString(),
        ]),
      ];
    }

    $element['#attached']['library'][] = 'vib_widget/select2';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $parents = $form['#array_parents'];
    $parents[] = $this->fieldDefinition->getName();

    $values = NestedArray::getValue($form_state->getUserInput(), $parents);
    // If this is a paragraph or some other widget, remove 'widget' from parents.
    if (empty($values)) {
      if (($key = array_search('widget', $parents)) !== FALSE) {
        unset($parents[$key]);
      }
      $values = NestedArray::getValue($form_state->getUserInput(), $parents);
    }

    foreach ($values as &$item) {
      if (!isset($item['widget_properties'])) {
        continue;
      }

      // Make sure empty values are null.
      foreach ($item['widget_properties'] as &$widget_value) {
        if ($widget_value === '') {
          $widget_value = NULL;
        }
      }
    }

    return $values;
  }

  /**
   * Ajax callback to refresh widget properties.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response object.
   */
  public function ajaxRefreshWidgetProperties(array $form, FormStateInterface $form_state, Request $request) {
    $response = new AjaxResponse();
    $element_parents = explode('/', $request->query->get('element_parents'));

    $triggering_element = $form_state->getTriggeringElement();
    $parents = $triggering_element['#array_parents'];
    array_pop($parents);

    $element = NestedArray::getValue($form, $parents);

    $response->addCommand(new ReplaceCommand('fieldset[data-vib-widget-id="' . $this->generateVibWidgetId($element_parents) . '"]', $element));

    return $response;
  }

  /**
   * Generates unique id.
   *
   * @param array $parents
   *    The array of parents of the field.
   * @return string
   *    The generated id.
   */
  protected function generateVibWidgetId(array $parents) {
    $id = array_map(function ($parent) {
      return (is_numeric($parent)) ? $parent : Html::cleanCssIdentifier($parent);
    }, $parents);

    return implode('-', $id);
  }

}
