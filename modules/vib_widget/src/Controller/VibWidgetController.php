<?php

namespace Drupal\vib_widget\Controller;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\vib_widget\VibWidgetsClient;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines vib widget routes.
 */
class VibWidgetController extends ControllerBase {

  /**
   * The VIB widgets client.
   *
   * @var \Drupal\vib_widget\VibWidgetsClient
   */
  protected VibWidgetsClient $client;

  /**
   * The default cache bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * Constructs the controller.
   *
   * @param \Drupal\vib_widget\VibWidgetsClient $client
   *   The VIB widgets client.
   */
  public function __construct(VibWidgetsClient $client, CacheBackendInterface $cache) {
    $this->client = $client;
    $this->cache = $cache;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('vib_widget.client'),
      $container->get('cache.default')
    );
  }

  /**
   * Attempts to retrieve widget options for the given parameters.
   *
   * @param string $type
   *   The selectbox type.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getWidgetOptions(string $type, Request $request) {
    // Filter out all non-options, and make sure they're not empty strings.
    $options = array_filter($request->query->all(), function ($value, $key) {
      return !empty($value) && $value != 0 && in_array($key, [
        'page',
        'pageSize',
        'keyword',
        'selected',
      ]);
    }, ARRAY_FILTER_USE_BOTH);

    $page_size = $options['pageSize'] ?? 100;

    // Create a cache key with hashed options.
    $cache_key = 'vib_widget.type:' . $type . '.options:' . md5(serialize($options));
    if (!$cached_options = $this->cache->get($cache_key)) {
      $response = $this->client->getWidgetOptions(
        $type,
        $page_size,
        $options['page'] ?? 0,
        $options['keyword'] ?? NULL,
        $options['selected'] ?? NULL
      );

      $select2_options = [];
      foreach ($response as $id => $value) {
        $select2_options[] = [
          'id' => $id,
          'text' => $value,
        ];
      }

      // Cache for up to 5 minutes so repeated usage does not result in a crazy
      // amount of calls to the API endpoint.
      $this->cache->set($cache_key, $select2_options, time() + 300);
      $cached_options = new \stdClass();
      $cached_options->data = $select2_options;
    }

    $select2_options = $cached_options->data;
    $has_more_results = count($select2_options) >= $page_size;

    // Remove selected result from result set because the selected result is
    // already present in the original result set, which is fetched directly
    // rather than passing through this middleware.
    if (is_array($options['selected'])) {
      foreach ($options['selected'] as $option) {
        if (isset($select2_options[$option])) {
          unset($select2_options[$option]);
        }
      }
    }
    elseif (isset($options['selected']) && isset($select2_options[$options['selected']])) {
      unset($select2_options[$options['selected']]);
    }

    return new JsonResponse([
      'results' => $select2_options,
      'pagination' => [
        'more' => $has_more_results,
      ],
    ]);
  }

}
