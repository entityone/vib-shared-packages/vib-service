<?php


namespace Drupal\vib_widget;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class WidgetProperty.
 *
 * @package Drupal\vib_widget
 */
class WidgetProperty {

  use StringTranslationTrait;

  /**
   * The title.
   *
   * @var string
   */
  protected $title;

  /**
   * The attribute.
   *
   * @var string
   */
  protected $attribute;

  /**
   * The type.
   *
   * @var string
   */
  protected $type;

  /**
   * The default value.
   *
   * @var string
   */
  protected $default;

  /**
   * Is field multiple.
   *
   * @var boolean
   */
  protected $multiple;

  /**
   * WidgetProperty constructor.
   *
   * @param string $title
   *   The title.
   * @param string $attribute
   *   The attribute.
   * @param string $type
   *   The type.
   * @param string $default
   *   The default value.
   */
  public function __construct(string $title, string $attribute, string $type, string $default, bool $multiple) {
    $this->title = $title;
    $this->attribute = $attribute;
    $this->type = $type;
    $this->default = $default;
    $this->multiple = $multiple;
  }

  /**
   * @return string
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * @return string
   */
  public function getAttribute(): string {
    return $this->attribute;
  }

  /**
   * @return string
   */
  public function getType(): string {
    return $this->type;
  }

  /**
   * @return string
   */
  public function getDefault(): string {
    return $this->default;
  }

  /**
   * @return bool
   */
  public function isMultiple(): bool {
    return $this->multiple;
  }

  /**
   * @return array
   */
  public function buildElement($default_value) {
    $strategy = new WidgetPropertyStrategy($this);

    $build = [
      '#type' => $strategy->getElementType(),
      '#title' => $this->getTitle(),
      '#multiple' => $this->isMultiple(),
    ];

    if ($options = $strategy->getOptions($default_value)) {
      $build['#options'] = $options;
      $build['#empty_option'] = $this->t('- Choose -');
      $build['#attributes']['data-widget-type'] = $strategy->getType();
      $build['#attributes']['data-default-value'] = $default_value;
      $build['#attributes']['class'][] = 'vib-widget-select2';
      $build['#chosen'] = FALSE;
      $build['#validated'] = TRUE; // Otherwise the infinite scroll select2 field will fail.
    }

    return $build;
  }

}
