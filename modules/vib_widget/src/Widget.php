<?php


namespace Drupal\vib_widget;

/**
 * Class Widget.
 *
 * @package Drupal\vib_widget
 */
class Widget {

  /**
   * The ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The name.
   *
   * @var string
   */
  protected $name;

  /**
   * The description.
   *
   * @var string
   */
  protected $description;

  /**
   * Whether the widget is multilingual.
   *
   * @var bool
   */
  protected $multilingual;

  /**
   * The icon.
   *
   * @var string
   */
  protected $icon;

  /**
   * The javascript references.
   *
   * @var array
   */
  protected $js;

  /**
   * The CSS references.
   *
   * @var array
   */
  protected $css;

  /**
   * The properties.
   *
   * @var \Drupal\vib_widget\WidgetProperty[]
   */
  protected $properties;

  /**
   * Widget constructor.
   *
   * @param string $id
   *   The ID.
   * @param string $name
   *   The name.
   * @param string $description
   *   The description.
   * @param bool $multilingual
   *   Whether the widget is multilingual.
   * @param string $icon
   *   The icon.
   * @param array $js
   *   The javascript references.
   * @param array $css
   *   The CSS references.
   * @param \Drupal\vib_widget\WidgetProperty[] $properties
   *   The properties.
   */
  public function __construct(string $id, string $name, string $description, bool $multilingual, string $icon, array $js, array $css, array $properties) {
    $this->id = $id;
    $this->name = $name;
    $this->description = $description;
    $this->multilingual = $multilingual;
    $this->icon = $icon;
    $this->js = $js;
    $this->css = $css;
    $this->properties = $properties;
  }

  /**
   * Returns the ID.
   *
   * @return string
   *   The ID.
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * Returns the name.
   *
   * @return string
   *   The name.
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * Returns the description.
   *
   * @return string
   *   The description.
   */
  public function getDescription(): string {
    return $this->description;
  }

  /**
   * Returns the description.
   *
   * @return string
   *   The description.
   */
  public function getMultilingual(): string {
    return $this->multilingual;
  }

  /**
   * Returns the icon.
   *
   * @return string
   *   The icon.
   */
  public function getIcon(): string {
    return $this->icon;
  }

  /**
   * Returns the javascript references.
   *
   * @return array
   *   The javascript references.
   */
  public function getJs(): array {
    return $this->js;
  }

  /**
   * Returns the CSS references.
   *
   * @return array
   *   The CSS references.
   */
  public function getCss(): array {
    return $this->css;
  }

  /**
   * Returns the library name.
   *
   * @return string
   *   The library name.
   */
  public function getLibraryName(): string {
    return 'vib-widgets.' . $this->getId();
  }

  /**
   * Returns the properties.
   *
   * @return \Drupal\vib_widget\WidgetProperty[]
   *   The properties.
   */
  public function getProperties(): array {
    return $this->properties;
  }

  /**
   * Return instance by given definition.
   *
   * @param array $definition
   *   The definition.
   *
   * @return \Drupal\vib_widget\Widget
   *   The widget.
   */
  public static function createFromDefinition(array $definition) {
    $properties = [];
    foreach ($definition['properties'] as $property) {
      $required_keys = ['title', 'attribute', 'type', 'default'];
      foreach ($required_keys as $key) {
        if (!isset($property[$key])) {
          throw new \InvalidArgumentException($key . 'is required');
        }
      }

      $properties[] = new WidgetProperty($property['title'], $property['attribute'], $property['type'], $property['default'], $property['multiple'] ?? FALSE);
    }

    return new static(
      $definition['id'],
      $definition['name'],
      $definition['description'],
      $definition['multilingual'] ?? FALSE,
      $definition['icon'],
      $definition['js'],
      $definition['css'],
      $properties
    );
  }

}
