<?php

namespace Drupal\vib_widget;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\State\StateInterface;
use Drupal\vib_service\Client\VibClientException;
use GuzzleHttp\RequestOptions;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class VibWidgetsClient.
 *
 * @package Drupal\vib_widget
 */
class VibWidgetsClient {

  const STATE_BEARER_TOKEN = 'vib_services_widgets_bearer_token';

  /**
   * The config factory.
   *
   * @var ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The client factory.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * The http client.
   *
   * @var array|\GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The state machine
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a VibWidgetsClient object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   The client factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state machine.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientFactory $http_client_factory, StateInterface $state) {
    $this->configFactory = $config_factory;
    $this->httpClientFactory = $http_client_factory;
    $this->httpClient = $this->createFromOptions();
    $this->state = $state;
  }

  /**
   * @return array|\GuzzleHttp\Client
   */
  protected function createFromOptions() {
    $client = drupal_static(__FUNCTION__);

    if (!isset($client)) {
      $endpoint = $this->configFactory->get('vib_widget.settings')
        ->get('api_endpoint');
      $options = [
        'base_uri' => substr($endpoint, -1, 1) == '/' ? $endpoint : $endpoint . '/',
        'headers' => [
          'Content-Type' => 'application/json',
        ]
      ];

      $client = $this->httpClientFactory->fromOptions($options);
    }

    return $client;
  }

  /**
   * Refreshes bearer token.
   *
   * @param string $scope
   *   The scope.
   *
   * @return string
   *   The bearer token.
   *
   * @throws \Drupal\vib_service\Client\VibClientException
   */
  protected function apiRefreshBearerToken(string $scope) {
    $config = $this->configFactory->get('vib_service.settings');
    try {
      $request_options = [
        'form_params' => [
          'client_id' => $config->get('client_id'),
          'client_secret' => $config->get('client_secret'),
          'grant_type' => 'client_credentials',
          'scope' => $scope,
        ],
      ];

      $response = $this->httpClientFactory->fromOptions()
        ->request('POST', $config->get('token_endpoint'), $request_options);
      $json = Json::decode($response->getBody()->getContents());

      if (empty($json['access_token'])) {
        throw new VibClientException('Could not API fetch bearer token');
      }

      return $json['access_token'];
    } catch (\Exception $e) {
      throw new VibClientException($e->getMessage());
    }
  }

  /**
   * Returns (cached) bearer token.
   *
   * @param string $scope
   *   The scope.
   *
   * @return string
   *   The bearer token.
   *
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function apiBearerToken($scope = 'api.catalog.lookups') {
    if (!$tokens = $this->state->get(self::STATE_BEARER_TOKEN)) {
      $token = $this->apiRefreshBearerToken($scope);
      // Cache token per scope.
      $tokens[$scope] = $token;
      $this->state->set(self::STATE_BEARER_TOKEN, $tokens);
    }

    if (empty($tokens[$scope])) {
      $token = $this->apiRefreshBearerToken($scope);
      // Cache token per scope.
      $tokens[$scope] = $token;
      $this->state->set(self::STATE_BEARER_TOKEN, $tokens);
    }

    return $tokens[$scope];
  }

  /**
   * @param $method
   * @param $path
   * @param array $options
   * @param bool $return_as_json
   * @return array|\GuzzleHttp\Psr7\Response
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function request($method, $path, $options = [], $return_as_json = TRUE) {
    $cache_id = $path . Json::encode($options);
    $cache = &drupal_static(__FUNCTION__);

    if ($method === 'GET' && isset($cache[$cache_id])) {
      return $cache[$cache_id];
    }

    // Keep track of loops to make sure we do not stay here forever.
    // This can occur when VIB services would be "down".
    if (!$loops = &drupal_static(__FUNCTION__ . 'loops')) {
      $loops = 1;
    }
    try {
      // Add Bearer token.
      $options['headers']['Authorization'] = 'Bearer ' . $this->apiBearerToken();

      /** @var \GuzzleHttp\Psr7\Response $response */
      $response = $this->httpClient->request($method, $path, $options);
    } catch (\Exception $e) {
      if ($e->getCode() === 401 && $loops < 3) {
        // Bearer token is probably expired.
        // Delete expired one and execute request again.
        $this->state->delete(self::STATE_BEARER_TOKEN);
        $loops++;
        return $this->request($method, $path, $options, $return_as_json);
      }

      throw new VibClientException($e->getMessage());
    }

    if ($response->getStatusCode() == 200 || $response->getStatusCode() == 201) {
      if ($return_as_json) {
        // Cache response and return it.
        $cache[$cache_id] = Json::decode($response->getBody()->getContents());
        return $cache[$cache_id];
      }
      return $response;
    }

    return [];
  }

  /**
   * @param string $type
   * @param int $page_size
   * @param int $page
   * @param string|null $keyword
   * @param string|int|null $default_value
   *
   * @return array|\GuzzleHttp\Psr7\Response
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getWidgetOptions(string $type, int $page_size = 100, int $page = 0, ?string $keyword = NULL, $default_value = NULL) {
    $options = [
      RequestOptions::QUERY => [
        'type' => $type,
        'pageSize' => $page_size,
        'page' => $page,
      ],
    ];

    // Add keyword if people search for a certain result.
    if (!empty($keyword)) {
      $options[RequestOptions::QUERY]['searchTerm'] = $keyword;
    }

    if ($page === 0) {
      // The widgets API does not allow page 0.
      unset($options[RequestOptions::QUERY]['page']);

      // Only add the default (selected) value on the first page, so we filter
      // out repeated/duplicate values.
      if (!empty($default_value)) {
        // Make sure the default value is in the correct format. An array should be a comma separated string list.
        if (is_array($default_value)) {
          $default_value = implode(',', $default_value);
        }
        $options[RequestOptions::QUERY]['selected'] = $default_value;
      }
    }

    return $this->request('GET', 'widget-options', $options);
  }

  /**
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getWidgetsDefinition() {
    $client = $this->httpClientFactory->fromOptions([
      'base_uri' => $this->configFactory->get('vib_widget.settings')
        ->get('widget_definition'),
    ]);

    /** @var \GuzzleHttp\Psr7\Response $response */
    $response = $client->request('GET');

    if ($response->getStatusCode() !== 200 && $response->getStatusCode() !== 201) {
      // Invalid status code.
      throw new BadRequestHttpException();
    }

    return Json::decode($response->getBody()->getContents());
  }

}
