<?php

namespace Drupal\vib_widget\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\State\StateInterface;
use Drupal\vib_widget\VibWidgetHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VibWidgetSettingsForm.
 *
 * @package Drupal\vib_widget\Form
 */
class VibWidgetSettingsForm extends ConfigFormBase {

  /**
   * THe widget helper.
   *
   * @var \Drupal\vib_widget\VibWidgetHelper
   */
  protected $widgetHelper;

  /**
   * Constructs a VibWidgetSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\vib_widget\VibWidgetHelper $widget_helper
   *   The widget helper.
   */
  public function __construct(ConfigFactoryInterface $config_factory, VibWidgetHelper $widget_helper) {
    parent::__construct($config_factory);
    $this->widgetHelper = $widget_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('vib_widget.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vib_widget_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['vib_widget.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('vib_widget.settings');

    $form['api_endpoint'] = [
      '#title' => $this->t('API endpoint'),
      '#type' => 'textfield',
      '#default_value' => $settings->get('api_endpoint'),
      '#description' => $this->t('Endpoint to execute API calls'),
      '#required' => TRUE,
    ];

    $form['widget_definition'] = [
      '#title' => $this->t('Widget definition'),
      '#type' => 'url',
      '#default_value' => $settings->get('widget_definition'),
      '#description' => $this->t('The widget CDN endpoint, eg "https://viblibrary-staging.azurewebsites.net/embed/vib-widgets-staging.json"'),
      '#required' => TRUE,
    ];

    $form['current_definition'] = [
      '#type' => 'details',
      '#title' => $this->t('Current definition'),
      '#open' => FALSE,
      [
        '#type' => 'inline_template',
        '#template' => '<pre><code>{{ code }}</code></pre>',
        '#context' => [
          'code' => Json::encode($this->widgetHelper->getWidgetsDefinition()),
        ],
      ],
    ];

    $form = parent::buildForm($form, $form_state);
    $form['actions']['submit']['#value'] = $this->t('Save configuration and refresh widgets definition');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('vib_widget.settings');

    $settings
      ->set('api_endpoint', $form_state->getValue('api_endpoint'))
      ->set('widget_definition', $form_state->getValue('widget_definition'))
      ->save();

    // Refresh widgets definition.
    $this->widgetHelper->refreshWidgetsDefinition();

    // Clear caches to attach new library files.
    drupal_flush_all_caches();

    parent::submitForm($form, $form_state);
  }

}
