<?php


namespace Drupal\vib_widget;

use Drupal\vib_service\Client\Model\Request\VibUserListRequest;

/**
 * Class WidgetPropertyStrategy.
 *
 * @package Drupal\vib_widget
 */
class WidgetPropertyStrategy {

  /**
   * The widget property.
   *
   * @var \Drupal\vib_widget\WidgetProperty
   */
  protected $widgetProperty;

  /**
   * The vib service client.
   *
   * @var \Drupal\vib_widget\VibWidgetsClient
   */
  protected $client;

  /**
   * WidgetPropertyOptionsStrategy constructor.
   *
   * @param \Drupal\vib_widget\WidgetProperty $property
   *   The property.
   */
  public function __construct(WidgetProperty $property) {
    $this->widgetProperty = $property;
    $this->client = \Drupal::service('vib_widget.client');
  }

  /**
   * Returns the widget type.
   *
   * @return string
   */
  public function getType() {
    return $this->widgetProperty->getType();
  }

  /**
   * @return string
   */
  public function getElementType() {
    if ($this->widgetProperty->getType() === 'bool') {
      return 'radios';
    }
    if ($this->getType() === 'text') {
      return 'textfield';
    }

    return 'select';
  }

  /**
   * Returns array of options for the widget property.
   * Return [] for type text (VIBSBSUP-511)
   *
   * @return array
   */
  public function getOptions($default_value = NULL) {
    switch ($this->widgetProperty->getType()) {
      case 'bool':
        return [
          'true' => 'Yes',
          'false' => 'No',
        ];
      case 'text':
        return [];
      default:
        return $this->client->getWidgetOptions($this->widgetProperty->getType(), 25, 0, NULL, $default_value);
      }
    }
}
