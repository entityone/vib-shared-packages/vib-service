This module integrates Drupal with the VIB widgets

# Installation

* Install module by running `drush en vib_widget`

# Configuration

* Navigate to `/admin/config/services/vib-service/widgets`, and fill out the form
* Add a field of type `VIB Widget` to the content entity type you want to render widgets on
* Fill out the field and the widget will be embedded accordingly
